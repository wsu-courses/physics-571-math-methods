(sec:readings)=
Resources, Readings, and References
===================================

## Textbook

The textbooks for this course are listed in the {ref}`sec:Syllabus`.

## Differential Equations
ODEs are tricky, and it helps to be familiar some of the available tables of exact
solutions like the following:

* "Handbook of Exact Solutions for Ordinary Differential Equations":
  {cite}`Polyanin:2003`.
  

(sec:linear_algebra_resources)=
## Linear Algebra

* [Essence of linear algebra][]: A great set of highly visual videos by [3Blue1Brown][]
  getting you up to abstract vector spaces.
* [MIT 18.06 Linear Algebra]: A set of [video
  lectures](https://ocw.mit.edu/courses/18-06-linear-algebra-spring-2010/video_galleries/video-lectures/)
  and accompanying material for the MIT Linear Algebra course.
* [Qiskit Linear Algebra](https://qiskit.org/textbook/ch-appendix/linear_algebra.html):
  A short introduction that is part of the [Qiskit][] platform.
* Appendix A of {cite:p}`Mermin:2007` has a nice short review of Dirac notation.


(sec:references)=
References
==========

```{bibliography}
:style: alpha
```


[MIT 18.06 Linear Algebra]: <https://ocw.mit.edu/courses/18-06-linear-algebra-spring-2010/>
[3Blue1Brown]: <https://www.youtube.com/c/3blue1brown>
[Essence of linear algebra]: <https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab>
[Qiskit]: https://qiskit.org
