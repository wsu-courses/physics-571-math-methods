---
jupytext:
  formats: ipynb,md:myst,py:light
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
FIG_DIR = Path(mmf_setup.ROOT) / '../Docs/_build/figures/'
os.makedirs(FIG_DIR, exist_ok=True)
import logging; logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
try: from myst_nb import glue
except: glue = None
```

(sec:Tensors)=
# 4. Tensors and Manifolds

This is my attempt to unify the geometric picture of tensors and manifolds with the
practical aspects of "index pushing" needed to do physics.  I am attempting to get to
the point here, so rigor will be lacking: please see {cite}`Hassani:2013` for careful
definitions.  This largely replaces the first part of Chapter 4 of {cite}`Arfken:2013`.

## Essential Definitions

An $n$-dimensional (differentiable) [manifold][] $M$ is a set that every locally looks like
Euclidian space $\mathbb{R}^{n}$.  To work with manifolds, we need a collection of
**charts** called an **atlas** which maps parts of the manifold to subsets of
$\mathbb{R}^{n}$.

:::{margin}
It turns out that nice $m$-dimensional manifolds can always be embedded in $M \subset
\mathbb{R}^{2m}$. See the [Whitney embedding theorem][].
:::
For the purposes of this discussion, we will consider manifolds that are **embedded** in
a higher dimensional Euclidian space $M\subset \mathbb{R}^{m}$.  This embedding will
allow use to determine the **metric** $\op{g}$, but it is not necessary that manifolds be
embedded (or even that they can be embedded, though most can).  These manifolds are call
[Riemannian manifolds][].
:::{important}
All of the information about the nature of the manifold is contained in the metric
$\op{g}$.  Thus: even if we don't have (or know) the embedding, we can still work with
the manifold if we know $\op{g}$.

We will start with embedded manifolds, but once we develop the formalism, we will be
able to describe, e.g., spacetime, where we cannot "get out" to see the embedding.  For
example, consider a sphere like the surface of the earth.  From space we can see that
the surface is curved, and we can calculate anything we like using Euclidian geometry,
but as people living on the earth, we must infer properties like the curvature by making
local measurements within the manifold itself.
:::

:::{figure} https://upload.wikimedia.org/wikipedia/commons/9/9a/Nondifferentiable_atlas.png
:figclass: margin
Example of different charts for describing the surface of the earth -- an
$m=2$-dimensional manifold embedded in $n=3$-dimensional space $\mathbb{R}^3$.
:::

## Embedded Manifolds
To be concrete, we start with an embedded $m$-dimensional manifold $M\subset
\mathbb{R}^{n}$ where $n\geq m$.  This allows us to use Cartesian coordinates $x
\in \mathbb{R}^{n}$ to describe points on the manifold.  To work on the manifold,
however, we would like to use a different set of coordinates $X \in
\mathbb{R}^{m}$.

:::{admonition} Notation
Since there are $m$ coordinates $X^{\alpha}$, it is tempting to use the notation
$\vect{X}$ to describe the points.  I will try to refrain from doing this, however,
since these points are **not vectors** in the sense of the manifold -- it make no sense
to "add" two points on a sphere for example.  Instead I will just use $x$ and $X$ to
represent these, and reserve vector notation for the vector spaces like the **tangent
space**.

For the actual **vectors** and **covectors** I will use bra-ket notation:
\begin{gather*}
  \newcommand{\uket}[1]{\ket{\hat{#1}}}
  \newcommand{\ubra}[1]{\bra{\hat{#1}}}
  \newcommand{\pddiff}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
  \ket{v} = \ket{X_\alpha}V^{\alpha}, \qquad
  \bra{v} = V_{\alpha}\bra{X^\alpha},
\end{gather*}
where $\{\ket{X_\alpha}\}$ is a set of basis vectors and $\{\bra{X^\alpha}\}$ is a basis of
covectors (forms).  Here an everywhere unless explicitly stated otherwise, we will use
the Einstein summation convention of summing over repeated indices.

In this discussion, we will consider primarily two sets of coordinates: Euclidean
coordinates in the embedding space $x^{i}$, and general coordinates in the chart $X^{\alpha}$.
To help distinguish between these, we will use lower-case letters $x$ and latin indices
$i$, $j$, $k$, $l$, etc. for the Euclidean coordinates, and upper case letters $X$ with
greek indices $\alpha$, $\beta$, $\gamma$, $\delta$ etc. for the chart coordinates
$X^{\alpha}$.  This choice is so that the usual Euclidean coordinates $x^1$, $x^2$ look
familiar.

:::

To describe the manifold, we can define the $n$ functions
\begin{gather*}
  x^{i}(X^1, X^2, \dots, X^{m}). 
\end{gather*}
This provides a complete description of the manifold and its properties everywhere these
functions are not singular.
:::{admonition} Concrete Examples: Polar coordinates and a unit sphere
:class: dropdown

To keep things real, we will follow two concrete examples:
* **Polar coordinates** for describing the plane $\mathbb{R}^2$ with $m=n=2$.

  Here $(x^1, x^2) = (x, y)$ and $(X^1, X^2) = (r, \phi)$:
  \begin{align*}
    x^1(X^1, X^2) &= x(r, \phi) = r\cos\phi = X^1 \cos X^2,\\
    x^2(X^1, X^2) &= y(r, \phi) = r\sin\phi = X^1 \sin X^2.
  \end{align*}
  Inverting these
  \begin{align*}
    X^1(x^1, x^2) &= r(x, y) = \sqrt{(x)^2 + (y)^2} = \sqrt{(x^1)^2 + (x^2)^2},\\
    X^2(x^1, x^2) &= \phi(x, y) = \tan^{-1}\frac{y}{x} = \tan^{-1}\frac{x^2}{x^1}.
  \end{align*}
   
* **Unit sphere**: Spherical coordinates for describing points on the surface of the
  unit sphere with $m=2$ embedded in $n=3$.

  Here $(x^1, x^2, x^3) = (x, y, z)$ and $(X^1, X^2) = (\theta, \phi)$:
  \begin{align*}
    x^1(X^1, X^2) &= x(\theta, \phi) = \sin\theta\cos\phi = \sin X^1 \cos X^2,\\
    x^2(X^1, X^2) &= y(\theta, \phi) = \sin\theta\sin\phi = \sin X^1 \sin X^2,\\
    x^3(X^1, X^2) &= z(\theta, \phi) = \cos\theta = \cos X^1.
  \end{align*}
  Inverting these
  \begin{align*}
    X^1(x^1, x^2, x^3) &= \theta(x, y, z) = \cos^{-1}z = \cos^{-1}x^3,\\
    X^2(x^1, x^2, x^3) &= \phi(x, y, z) = \tan^{-1}\frac{y}{x} = \tan^{-1}\frac{x^2}{x^1}.
  \end{align*}
  Restricted to the sphere:
  \begin{gather*}
    x^2 + y^2 + z^2 = 1,\\
    x^2 + y^2 = 1 - z^2 = 1-\cos^2\theta = \sin^2\theta.
  \end{gather*}
  
Note that neither of these **charts** is sufficient to fully describe the full manifold
since they are singular at e.g. $r=0$ and $\theta = 0$.  The spherical chart is also not
properly differentiable at the boundaries -- i.e. physically the points $\phi$ and
$\phi+2\pi$ should be identified, but according to the chart, they are different.

We will restrict our attention to a **submanifold** that is simply connected and has no
singularities for this discussion.
:::

:::{margin}
Note: we follow the order here described in [Jacobian matrix][] which is the transpose
of what {cite}`Arfken:2013` uses in e.g. Eq. (4.74).
:::
## Jacobian Matrix
In particular, properties of the embedding space can be converted to properties of the
manifold through the **Jacobian matrix** $\mat{J}$ and its inverse $\mat{J}^{-1}$:
:::{margin}
We introduce a shorthand notation here for representing the partial derivative
\begin{gather*}
  \pdiff{X^{i}}{x^\alpha} = X^{i}{}_{,\alpha}.  
\end{gather*}
We will explain the lower placement a little later.  (Derivatives act as forms.)
:::
\begin{gather*}
  \mat{J}^{-1} = \pdiff{X}{x} = \pdiff{(X^1, X^2, \dots, X^m)}{(x^1, x^2, \dots, x^n)},
  \qquad [\mat{J}^{-1}]^{i}{}_{\alpha} = \pdiff{X^i}{x^{\alpha}} = X^{i}{}_{,\alpha}\\
  \mat{J} = \pdiff{x}{X} = \pdiff{(x^1, x^2, \dots, x^n)}{(X^1, X^2, \dots, X^m)}, 
  \qquad J^{i}{}_{\alpha} = \pdiff{x^i}{X^\alpha} = x^{i}{}_{,\alpha}.
\end{gather*}
Note that if we think of $x^i = f(X)$ as a vector valued function, then $\mat{J} =
\mat{\d} f$ is the differential such that
\begin{gather*}
  x(X+\epsilon) = x(X) + \mat{J}\epsilon + O(\epsilon^2).
\end{gather*}
In matrix form:
\begin{gather*}
  \mat{J} = \begin{pmatrix}
    \pdiff{x^1}{X^1} & \pdiff{x^1}{X^2} & \dots & \pdiff{x^1}{X^{m}}\\
    \pdiff{x^2}{X^1} & \pdiff{x^2}{X^2} & \dots & \pdiff{x^2}{X^{m}}\\
    \pdiff{x^3}{X^1} & \pdiff{x^3}{X^2} & \dots & \pdiff{x^3}{X^{m}}\\
    \vdots & \vdots & & \vdots\\
    \pdiff{x^{n}}{X^1} & \pdiff{x^{n}}{X^2} & \dots & \pdiff{x^{n}}{X^{m}}
  \end{pmatrix},\qquad
  \mat{J}^{-1} = \begin{pmatrix}
    \pdiff{X^1}{x^1} & \pdiff{X^1}{x^2} & \pdiff{X^1}{x^3} &\dots & \pdiff{X^1}{x^{n}}\\
    \pdiff{X^2}{x^1} & \pdiff{X^2}{x^2} & \pdiff{X^2}{x^3} & \dots & \pdiff{X^2}{x^{n}}\\
    \vdots & \vdots & \vdots & & \vdots\\
    \pdiff{X^{m}}{x^1} & \pdiff{X^{m}}{x^2} & \pdiff{X^{m}}{x^3} & \dots & \pdiff{X^{m}}{x^{n}}
  \end{pmatrix}.
\end{gather*}
Note that $\mat{J}^{-1}\mat{J} = \mat{1}$, but that $\mat{J}\mat{J}^{-1}$ may be rank
deficient if $m < n$.

:::{admonition} Concrete Examples: Polar coordinates and a unit sphere
:class: dropdown

* **Polar coordinates**:

  \begin{gather*}
    \mat{J} = \pdiff{(x, y)}{(r, \phi)} 
    =\begin{pmatrix}
      \cos\phi & -r\sin\phi \\
      \sin\phi & r\cos\phi
    \end{pmatrix}, \\
    \mat{J}^{-1} = \pdiff{(r, \phi)}{(x, y)} = \begin{pmatrix}
      x/r & y/r \\
      x/r^2 & -y/r^2
    \end{pmatrix}
    = \begin{pmatrix}
      \cos\phi & \sin\phi \\
      r^{-1}\cos\phi & -r^{-1}\sin\phi
    \end{pmatrix}
  \end{gather*}
  Notice that $\mat{J}^{-1}\mat{J} = \mat{J}\mat{J}^{-1} = \mat{1}$, justifying the nomenclature.
  
* **Unit sphere**:

  \begin{gather*}
    \mat{J} = \pdiff{(x, y, z)}{(\theta, \phi)}
    = \begin{pmatrix}
      \cos\theta\cos\phi & -\sin\theta\sin\phi \\
      \cos\theta \sin\phi & \sin\theta\cos\phi \\
      -\sin\theta & 0
    \end{pmatrix},\\
    \mat{J}^{-1} = \pdiff{(\theta, \phi)}{(x, y, z)} 
    = \begin{pmatrix}
      0 & 0 & \frac{-1}{\sqrt{1 - z^2}}\\
      \frac{-y}{x^2+y^2} & \frac{x}{x^2+y^2} & 0
    \end{pmatrix}
    = \begin{pmatrix}
      0 & 0 & \frac{-1}{\sin\theta}\\
      \frac{-\sin\phi}{\sin\theta} &  \frac{\cos\phi}{\sin\theta} &  0
    \end{pmatrix}.
  \end{gather*}
  \begin{gather*}
    \mat{J}\mat{J}^{-1} = \begin{pmatrix}
      \sin^2\phi & - \sin\phi\cos\phi & -\cot\theta \cos\phi \\
      -\sin\phi\cos\phi & \cos^2\phi & -\cot\theta\sin\phi\\
      0 & 0 & 1
    \end{pmatrix}.
  \end{gather*}
   Within the manifold, $\mat{J}^{-1}\mat{J} = \mat{1}$, but within the embedded space,
   $\mat{J}\mat{J}^{-1} \neq \mat{1}$ since the Jacobian matrix is rank deficient.
:::

## Vectors
Consider a particle moving on the manifold.  This motion can be described by the curve
with coordinates $x\bigl(X(t)\bigr)$ through the embedded space.  This curve defines the
velocity **vector** $\ket{v}$:
:::{margin}
Using the compact derivative notation, this looks like
\begin{gather*}
  \ket{v} = \ket{\hat{e}_i}\dot{x}^i = \ket{\hat{e}_i}x^{i}{}_{,\alpha}\dot{X}^\alpha.
\end{gather*}
:::
\begin{align*}
  \ket{v} 
  &= \ket{\hat{e}_i}\overbrace{\diff{x^i(t)}{t}}^{v^i}
  = \underbrace{\ket{\hat{e}_i}\overbrace{\pdiff{x^i}{X^\alpha}}^{[\mat{J}]^{i}{}_{\alpha}}}_{\ket{X_\alpha}}\overbrace{\diff{X^\alpha}{t}}^{V^\alpha}\\
  &= \ket{\hat{e}_i}v^i
  = \ket{X_\alpha} V^\alpha.
\end{align*}
Note: the **vector** $\ket{v}$ represents a "physical" object -- something real (as
defined by the embedding).  By contrast, the **components** $v^{i}$ and $V^{\alpha}$ are
the "columns of numbers" often associated with vectors.
\begin{gather*}
  \ket{v} = \Bigl(\begin{matrix}
    \ket{X_1} & \ket{X_2} & \cdots & \ket{X_m}
  \end{matrix}
  \Bigr)
  \underbrace{\begin{pmatrix}
    V^1\\
    V^2\\
    \vdots\\
    V^{m}
  \end{pmatrix}}_{V^\alpha}
\end{gather*}
Unlike the actual vector $\ket{v}$, these numbers depend on the coordinate choice.

:::{margin}
Our notation (top) corresponds to the following notation in {cite}`Arfken:2013` (bottom):
\begin{alignat*}{3}
  \ket{a} &= \ket{\hat{e}_i}a^{i} &&= \ket{X_\alpha}A^{\alpha}\\
  \boldsymbol{\mathrm{A}} &= 
  \hat{\boldsymbol{\mathrm{e}}}_i(A')^{i} &&= \boldsymbol{\varepsilon}_i A^{i}.
\end{alignat*}
Here are some equations translated to our notation:
\begin{gather*}
  \ket{X_{\alpha}} = \ket{\hat{e}_i}x^{i}{}_{,\alpha}\tag{4.37}\\
  \ket{A} = \ket{X_\alpha}A^{\alpha}\tag{4.38}\\
  g_{\alpha\beta} = \op{g}(\ket{X_\alpha},\ket{X_\beta})\tag{4.40}\\
\end{gather*}
For (4.40), keep in mind that we can compute this as the dot product in the embedding
space.
\begin{gather*}
  \bra{\nabla}\psi = \psi_{,\alpha}\bra{X^{\alpha}}\tag{4.50*}
\end{gather*}
*Note that (4.50) in the book is the dual of (4.50\*) in terms of vectors rather than
covectors, which is somewhat unnatural.*
:::
:::{admonition} Comparison with notation in {cite}`Arfken:2013`
:class: dropdown

Remember that here we use $v^{i}$ as the components of the vector $\ket{v}$ in Euclidean
embedding space, and $V^{\alpha}$ as the coefficients in the chart.  We are relying on
the type of indices to distinguish.  In contrast, {cite}`Arfken:2013` use a prime.
Thus, we have
\begin{gather*}
  \ket{\hat{e}_i}v^{i} \equiv \hat{\boldsymbol{e}}_i(A')^{i},\qquad
  \ket{X_\alpha}V^{\alpha} \equiv \boldsymbol{\varepsilon}_i A^i.
\end{gather*}
{cite}`Arfken:2013` also uses $\boldsymbol{\varepsilon}^i$ which would correspond to
$\ket{X^{\alpha}}=\ket{X_{\beta}}g^{\beta\alpha}$.  One can in principle use these, but
we instead express everything in the natural form where basis vectors always have lower
indices.  In our notations $\ket{X^{\alpha}}$ or $\bra{X_{\alpha}}$ would probably
indicate we have made a mistake.  See e.g. our comment about (4.50).
:::

The Jacobian matrix $\mat{J}$ plays two roles here:
\begin{gather*}
  v^i = J^{i}{}_{\alpha}V^{\alpha} = \pdiff{x^{i}}{X^{\alpha}}V^{\alpha}
  , \qquad
  \ket{X_\alpha} = \ket{\hat{e}_i} J^{i}{}_{\alpha}.
\end{gather*}
Acting to the right, it transforms the components of a vector in the chart $V^{\alpha}$ into
the coordinates in the embedding space $v^i$.  Acting to the left, (or equivalently,
the transpose $\mat{J}^T$ acting to the right), it transforms the basis vectors
$\ket{\hat{e}_i}$ from the embedding space into the coordinate basis vectors $\ket{X_\alpha}$ that
we can use to express vectors like the velocity in terms of its components $V^{\alpha} =
\dot{X}^\alpha$ in the chart coordinates.

:::{important}
* The basis vectors $\ket{X_\alpha}$ need not be orthonormal.  It is often convenient to
  choose coordinates where the basis is at least orthogonal as this makes the metric
  nice, but it is not necessary or assumed here.
* We are being careful about our index location.  The components of the contravariant
  **vectors** are expressed with an **upper index**: $V^{\alpha}$ (expressed in the chart)
  and $v^{i}$ (expressed in the embedding space).  To keep the index convention, we
  label the actual basis vectors with a lower index $\ket{X_{\alpha}}$.
:::

:::{admonition} Concrete Examples: Polar coordinates and a unit sphere
:class: dropdown
* **Polar coordinates**:

  \begin{gather*}
    \begin{pmatrix}
      \ket{r} &
      \ket{\phi}
    \end{pmatrix}
    = 
    \begin{pmatrix}
      \ket{\hat{x}} &
      \ket{\hat{y}}
    \end{pmatrix}
    \underbrace{
    \begin{pmatrix}
      \cos\phi & -r\sin\phi\\
      \sin\phi & r\cos\phi
    \end{pmatrix}}_{\mat{J}}\\
    \ket{r} = \ket{\hat{x}}\cos\phi +  \ket{\hat{y}}\sin\phi, \\
    \ket{\phi} = -\ket{\hat{x}}r\sin\phi +\ket{\hat{y}}r\cos\phi.
  \end{gather*}

* **Unit sphere**:
  \begin{gather*}
    \begin{pmatrix}
      \ket{\theta} & 
      \ket{\phi}
    \end{pmatrix}
    = 
    \begin{pmatrix}
      \ket{\hat{x}} &
      \ket{\hat{y}} &
      \ket{\hat{z}}
    \end{pmatrix}
    \underbrace{
    \begin{pmatrix}
      \cos\theta\cos\phi & -\sin\theta\sin\phi\\
      \cos\theta\sin\phi & \sin\theta\cos\phi\\
      -\sin\theta & 0
    \end{pmatrix}}_{\mat{J}}\\
    \ket{\theta} = 
    \ket{\hat{x}}\cos\theta\cos\phi + 
    \ket{\hat{y}}\cos\theta\sin\phi -
    \ket{\hat{z}}\sin\theta,
    \\
    \ket{\phi} =
    -\ket{\hat{x}}\sin\theta\sin\phi +
    \ket{\hat{y}}\sin\theta\cos\phi.
  \end{gather*}
:::


:::{figure} _static/figures/Covector.svg
:figclass: margin
:name: fig:Covector

Visual depiction of a **vector** $\ket{a}$ (top) and a **covector** $\bra{v}$
(middle). The bottom shows the covector $\bra{v}$ acting on the vector $\ket{a}$ to give
$\braket{v|a} = 2$.  Notice that the vector would pierce the stacked covector exactly two
times.
:::
## Covectors (Forms)

In addition to vectors, we also have **covectors** or **1-forms**: these are linear
operators which take vectors into numbers, and should be thought of as the manifold
generalization of row vectors $\bra{v}$:
\begin{gather*}
  \bra{v}\bigl(a\ket{a} + b\ket{b}\bigr) = 
  a\braket{v|a} + b\braket{v|b} \in \mathbb{R}.
\end{gather*}
Given a basis of covectors $\{\bra{X^{\alpha}}\}$, we can express a covector  $\bra{v}$ as
\begin{gather*}
  \bra{v} = V_\alpha\bra{X^\alpha}, \qquad
  \ket{v} = \ket{X_\alpha}V^\alpha.
\end{gather*}
Here we use **lower indices** for the coefficients $V_\alpha$ which called **covariant** in
contradistinction to the **contravariant** $V^\alpha$ with **raised indices** that describe the
**vector** (or contravariant vector) $\ket{v}$:
:::{important}
Without a metric, there is no way to relate vectors and covectors.  Geometrically **they
are different types of objects**. (See {numref}`fig:Covector`.) Thus, the contravariant
coefficients $V^{\alpha}$ of the vector bear no relationship with the covariant coefficients
$V_\alpha$ of the covector.
:::

:::{margin}
Given a basis $\{\bra{Y^{\alpha}}\}$, we can always form such a basis by inverting the
matrix $\mat{M}$:
\begin{gather*}
  [\mat{M}]^{\alpha}{}_{\beta} = \braket{Y^{\alpha}|X_{\beta}},\\
  \bra{X^{\alpha}} = [\mat{M}^{-1}]^{\alpha}{}_{\beta}\bra{Y^{\beta}}.
\end{gather*}
:::
We can choose the basis of covectors $\{\bra{X^{\alpha}}\}$ to be dual to the basis of
vectors $\{\ket{X_{\alpha}}\}$:
\begin{gather*}
  \braket{X^{\alpha}|X_{\beta}} = \delta^{\alpha}_{\beta},\qquad
  \braket{v|g} = V_{\alpha}\braket{X^\alpha|X_\beta}G^{\beta} = V_{\alpha}G^{\alpha}.
\end{gather*}
For this to be invariant under coordinate transforms, the covariant coefficients
$V_{\alpha}$ and dual basis vectors must transform with the inverse Jacobian
$\mat{J}^{-1}$:
\begin{gather*}
  v_{i} = V_{\alpha}[\mat{J}^{-1}]^{\alpha}{}_{i} = V_{\alpha}\pdiff{X^{\alpha}}{x^{i}},
  \qquad
  \bra{X^{\alpha}} = [\mat{J}^{-1}]^{\alpha}{}_{i}\bra{\hat{e}^{i}}.
\end{gather*}

:::{admonition} Concrete Examples: Polar coordinates and a unit sphere
:class: dropdown
* **Polar coordinates**:

  \begin{gather*}
    \begin{pmatrix}
      \bra{r}\\
      \bra{\phi}
    \end{pmatrix}
    = 
    \underbrace{
    \begin{pmatrix}
      \cos\phi & \sin\phi\\
      r^{-1}\cos\phi & -r^{-1}\sin\phi
    \end{pmatrix}}_{\mat{J}^{-1}}
    \begin{pmatrix}
      \bra{\hat{x}}\\
      \bra{\hat{y}}
    \end{pmatrix}\\
    \begin{aligned}
      \bra{r} &= \cos\phi\bra{\hat{x}} + \sin\phi\ket{\hat{y}}, \\
      \bra{\phi} &= r^{-1}\cos\phi\bra{\hat{x}} - r^{-1}\sin\phi\bra{\hat{y}}.
    \end{aligned}
  \end{gather*}

* **Unit sphere**:
  \begin{gather*}
    \begin{pmatrix}
      \bra{\theta}\\
      \bra{\phi}
    \end{pmatrix}
    = 
    \underbrace{
    \begin{pmatrix}
      0 & 0 & \frac{-1}{\sin\theta}\\
      \frac{-\sin\phi}{\sin\theta} & \frac{\cos\phi}{\sin\theta} & 0
    \end{pmatrix}}_{\mat{J}^{-1}}
    \begin{pmatrix}
      \bra{\hat{x}}\\
      \bra{\hat{y}}\\
      \bra{\hat{z}}
    \end{pmatrix},\\
    \begin{aligned}
      \bra{\theta} &= \frac{-\bra{\hat{z}}}{\sin\theta},\\
      \bra{\phi} &= \frac{-\sin\phi\bra{\hat{x}} + \cos\phi\bra{\hat{y}}}{\sin\theta}.
    \end{aligned}
  \end{gather*}
:::

## Completeness

Armed with these dual bases, we can express the identity operator as
\begin{gather*}
  \op{1} = \ket{X_\alpha}\bra{X^\alpha}, \qquad
  \op{1}\ket{v} = \ket{v}, \qquad
  \bra{v}\op{1} = \bra{v}.
\end{gather*}

## Transformations
:::{margin}
Once we have a metric to raise and lower indices, we will have
\begin{gather*}
  \Lambda_{\alpha}{}^{\beta} 
  = g_{\alpha\gamma}\Lambda^{\gamma}{}_{\delta}g^{\delta\beta},\\
  (\mat{\Lambda}^{-1})^T = \mat{g}\mat{\Lambda}\mat{g}^{-1},\\
  \mat{\Lambda}^{-1} = \mat{g}^{-1}\mat{\Lambda}^T\mat{g}.
\end{gather*}
In the familiar Euclidean context where $\mat{g} = \mat{g}^{-1} = \mat{1}$, we have
\begin{gather*}
  \mat{\Lambda}^{-1} = \mat{\Lambda}^T.
\end{gather*}
I.e., the set of transformations that are invariant with this constant metric are
orthogonal matrices $\mat{\Lambda} \in \mathrm{O}(m)$: rotations and reflections in 3D.
:::
The key property of vectors and covectors, and the defining properties of tensors, is
that their components **transform linearly under coordinate transformation**:
\begin{gather*}
  A^{\alpha} \rightarrow \Lambda^{\alpha}{}_{\beta}A^{\beta},\qquad
  B_{\alpha} \rightarrow \Lambda_{\alpha}{}^{\beta}B_{\beta}.
\end{gather*}
Invariance of the action of forms on vectors implies
\begin{gather*}
  \braket{b|a} = B_{\alpha}A^{\alpha} \rightarrow B_{\alpha}A^{\alpha} 
  = B_{\beta}\underbrace{\Lambda_{\alpha}{}^{\beta}
                         \Lambda^{\alpha}{}_{\gamma}}_{\delta^{\beta}_{\gamma}}A^{\gamma}
  \implies
  \Lambda_{\alpha}{}^{\beta}\Lambda^{\alpha}{}_{\gamma} = \delta^{\beta}_{\gamma}.
\end{gather*}
In matrix form:
\begin{gather*}
  \Lambda^{\alpha}{}_{\gamma} = [\mat{\Lambda}]^{\alpha}{}_{\gamma}, \qquad
  \Lambda_{\alpha}{}^{\beta} = [(\mat{\Lambda}^{-1})^T]_{\alpha}{}^{\beta}.
\end{gather*}
:::{admonition} Linearity
:class: dropdown
To see that vectors transform linearly, consider two set of coordinates
$X^{\alpha}(x)$ and $\tilde{X}^{\alpha}(x)$ as functions of the same embedding space
\begin{gather*}
  v^{i} 
  = V^{\alpha}\pdiff{x^{i}}{X^{\alpha}}
  = \tilde{V}^{\alpha}\pdiff{x^{i}}{\tilde{X}^{\alpha}}
  = \overbrace{\tilde{V}^{\beta}\pdiff{X^{\alpha}}{\tilde{X}^{\beta}}}^{V^{\alpha}}
    \pdiff{x^{i}}{X^{\alpha}},\\
  v_{i} 
  = V_{\alpha}\pdiff{X^{\alpha}}{x^{i}}
  = \tilde{V}_{\alpha}\pdiff{\tilde{X}^{\alpha}}{x^{i}}
  = \underbrace{\tilde{V}_{\beta}\pdiff{\tilde{X}^{\beta}}{X^{\alpha}}}_{V_{\alpha}}
    \pdiff{X^{\alpha}}{x^{i}},\\
  V^{\alpha} = \underbrace{\pdiff{X^{\alpha}}{\tilde{X}^{\beta}}}
                         _{\Lambda^{\alpha}{}_{\beta} = [\mat{J}]^{\alpha}{}_{\beta}}
               \tilde{V}^{\beta}, \qquad
  V_{\alpha} = \underbrace{\pdiff{\tilde{X}^{\beta}}{X^{\alpha}}}
                         _{\Lambda_{\alpha}{}^{\beta} = [\mat{J}^{-1}]^{\beta}{}_{\alpha}}
               \tilde{V}_{\beta}.
\end{gather*}
Thus, $\mat{\Gamma}$ is just the Jacobian $\mat{J}$ for the transformation between the
two coordinates $X(\tilde{X})$.  Thus, this is linear even if the coordinate
transformations are non-linear.
:::
If you have a collection of numbers that does not transform appropriately, then it
cannot be considered to be a tensor.

:::{margin}
For more rigorous details, see {cite}`Hassani:2013`.
:::

## Tensors
Let $\mathcal{V}$ denote a vector space (i.e., the space of tangent vectors at a fixed
point in the manifold), and let $\mathcal{V}^*$ denote the **dual space** of covectors.
A **tensor $\op{T}^{r}_{s}$ in $\mathcal{T}^{r}_{s}(\mathcal{V})$ on $\mathcal{V}$** is
a multilinear map
\begin{gather*}
  \op{T}^{r}_{s}: 
  \underbrace{\mathcal{V}^{*}\times\mathcal{V}^{*}\times\cdots\times\mathcal{V}^{*}}_{r}
  \times
  \underbrace{\mathcal{V}\times\mathcal{V}\times\cdots\times\mathcal{V}}_{s}
  \rightarrow \mathbb{R}.
\end{gather*}
Thus, covectors are tensors in $\mathcal{T}_{1}^{0}(\mathcal{V})$ since they take vectors into
numbers, and vectors are tensors in $\mathcal{T}^{1}_{0}(\mathcal{V})$ since they take
covectors into numbers.  Given a basis $\ket{X_{\alpha}}$ for $\mathcal{V}$ and a basis
$\bra{X^{\alpha}}$ for $\mathcal{V}^*$, the **components** of the tensor are
:::{margin}
Because of the ordering, we can safely omit the tensor products without ambiguity as
shown under the brace.
:::
\begin{gather*}
  A^{\alpha_1\dots\alpha_r}_{\beta_1\dots\beta_s} = 
  \op{A}\Bigl(\bra{X^{\alpha_1}},\dots,\bra{X^{\alpha_r}},
              \ket{X^{\beta_1}},\dots,\ket{X^{\beta_s}}\Bigr),
\end{gather*}
and we can write
\begin{gather*}
  \op{A} = A^{\alpha_1\dots\alpha_r}_{\beta_1\dots\beta_s}
    \underbrace{
       \ket{X_{\alpha_1}}\otimes\dots\otimes\ket{X_{\alpha_r}}\otimes
       \bra{X^{\beta_1}}\otimes\dots\otimes\bra{X^{\beta_s}}
    }_{\ket{X_{\alpha_1}}\dots\ket{X_{\alpha_r}}
       \bra{X^{\beta_1}}\dots\bra{X^{\beta_s}}
    }.
\end{gather*}

## Metric Tensor
A **metric tensor** (or **metric**) $\op{g}\in \mathcal{T}^{0}_{2}(\mathcal{V})$ acts as
an inner product on the vector space: it is bilinear operation that takes two vectors
into a real number, satisfying all of the properties of an [inner product][] (a.k.a. dot
product): 
\begin{gather*}
  \op{g}\bigl(\ket{a}, \ket{b}\bigr) = \braket{a|b}.
\end{gather*}
Thus, given a vector $\ket{v}$, the metric defines a linear operator that acts on
vectors returning numbers.  In other words, it maps the vector $\ket{v}$ to a covector
$\bra{v}$:
\begin{gather*}
  \bra{v} \equiv \op{g}\bigl(\ket{v}, \cdot\bigr), \qquad
  \braket{v|a} = \op{g}\bigl(\ket{v}, \ket{a}\bigr).
\end{gather*}
Note that the metric may be different at different points in the manifold: thus we
should think of **metric tensor field**. If the manifold is not embedded in another
space, then the metric provides all of the information about the manifold's structure. 

If the manifold is embedded, however, then we can use the Jacobian matrix $\mat{J}$ to
**induce** the metric from the embedding because the embedding space $\mathbb{R}^n$ has
a nature metric -- the Euclidean metric -- which we can use to induce the metric on the
manifold.

Consider the dot product of two vectors $\ket{a}$ and $\ket{b}$:
\begin{gather*}
  \ket{a} = \ket{X_\alpha}A^\alpha = \uket{e_i}J^{i}{}_{\alpha}A^{\alpha} = \uket{e_i}a^{i},\\
  \ket{b} = \ket{X_\alpha}B^\alpha = \uket{e_i}J^{i}{}_{\alpha}B^{\alpha} = \uket{e_i}b^{i}.
\end{gather*}
:::{margin}
You might be concerned about summing over tow raise indices here.  In general, you
should be concerned, but here the two indices $k$ correspond to the Euclidian embedding
space where the metric is trivial -- the identity $\delta_{ij}$.  Summing over these is
allowed, but only in the embedding space when computing the metric coefficients.  In the
chart coordinates, you should always ensure summed indices appear in pairs -- one up,
and one down.
:::
The inner product can now be induced from the embedding space:
\begin{gather*}
  \braket{a|b} = a^{i}\delta_{ij}b^{j} = a_{i}b^{i}
  = \Bigl(\begin{matrix}
    a^1 & \cdots & a^n
  \end{matrix}\Bigr)
  \begin{pmatrix}
    b^1 \\ \vdots \\ b^n
  \end{pmatrix}.
\end{gather*}
Note that the metric in the embedding space is just the identity $\delta_{ij}$, so the
covariant components equal the contravariant components: $a_i = a^i$.  This is
generally **not true** in the chart $A_\alpha \neq A^\alpha$.
Equating the inner product in the chart and the embedding space, we have
\begin{gather*}
  \braket{a|b} = \overbrace{a_{i}}^{J_{i\alpha}A^{\alpha}}\underbrace{b^{i}}_{J^{i}{}_{\alpha}B^\alpha}
  = A^{\alpha}\underbrace{J_{i\alpha}J^{i}{}_{\beta}}_{g_{\alpha\beta}}B^{\beta}.
\end{gather*}
Thus, we have
\begin{gather*}
  \braket{a|b} = A^{\alpha}g_{\alpha\beta}B^{\beta} \quad \text{where} \quad
  g_{\alpha\beta} = J_{i\alpha}J^{i}{}_{\beta} = [\mat{J}^T\mat{J}]_{\alpha\beta}.
\end{gather*}
Note that $g_{\alpha\beta} = g_{\beta\alpha}$ is symmetric.

Stated more directly
\begin{gather*}
  g_{\alpha\beta}= \op{g}\Bigl(\overbrace{\ket{X_{\alpha}}}^{\ket{\hat{e}_{i}}J^{i}{}_{\alpha}},
                               \overbrace{\ket{X_{\beta}}}^{\ket{\hat{e}_{j}}J^{j}{}_{\beta}}\Bigr)
  = J^{i}{}_{\alpha}\;\overbrace{
    \op{g}\Bigl(\ket{\hat{e}_{i}}, \ket{\hat{e}_{j}}\Bigr)
  }^{\braket{\hat{e}_i|\hat{e}_j}=\delta_{ij}}
  J^{j}{}_{\beta}
  = J^{i}{}_{\alpha}\delta_{ij}J^{j}{}_{\beta}
  = [\mat{J}^{T}\mat{J}]_{\alpha\beta}.
\end{gather*}
Armed with these coefficients, we can compute the **covariant coefficients** $A_\alpha$:
\begin{gather*}
  A_\alpha = g_{\alpha \beta}A^\beta
\end{gather*}
where we say that **the metric lowers the index**.  Note that these are the coefficients
of the covector $\bra{a}$:
\begin{gather*}
  \bra{a} = A_\alpha\bra{X^\alpha} = A^{\alpha}g_{\alpha\beta}\bra{X^\beta}.  
\end{gather*}
With this notation, we can write the **contraction**
\begin{gather*}
  \braket{A|B} = A_\alpha B^\alpha.
\end{gather*}
It is also useful to define a related set of numbers $g^{\alpha\beta}$ which **raise the index**,
taking covariant components back into contravariant components:
\begin{gather*}
  A^{\alpha} = g^{\alpha\beta}A_{\beta} = g^{\alpha\beta}g_{\beta \gamma}A^{\gamma}.
\end{gather*}
This must be the matrix inverse of $g_{\alpha\beta}$:
\begin{gather*}
  g^{\alpha\beta}g_{\beta \gamma} = \delta^{\alpha}_{\gamma}, \qquad
  g_{\alpha\beta} = [\mat{g}]_{\alpha\beta}, \qquad
  g^{\alpha\beta} = [\mat{g}^{-1}]_{\alpha\beta}.
\end{gather*}

:::{admonition} Notation of {cite}`Arfken:2013`
:class: dropdown
{cite}`Arfken:2013` uses the notation
\begin{gather*}
  \boldsymbol{\hat{e}_i}\equiv \uket{e_i}, \qquad
  \boldsymbol{\varepsilon_i}\equiv \ket{X_i}, \qquad
  \boldsymbol{\varepsilon^i}\equiv \ket{X^i},
\end{gather*}
and writes the calculation of $g_{ij}$ as
\begin{gather*}
  g_{ij} = \boldsymbol{\varepsilon_i}\cdot \boldsymbol{\varepsilon_j}.
\end{gather*}
In our notation, we would write this in terms of the **transpose** $\bra{X_\alpha} = \ket{X_\alpha}^T$:
\begin{gather*}
  \ket{X_\alpha} = \uket{e_i}J^{i}{}_{\alpha},\qquad
  \bra{X_\alpha} = \ubra{e_i}J^{i}{}_{\alpha},\\
  g_{\alpha \beta} = \braket{X_\alpha|X_\beta} = J^{k}{}_{\alpha}\braket{\hat{e_k}|\hat{e_l}}J^{l}{}_{\beta}
         = J^{k}{}_{\alpha}J^{k}{}_{\beta} = [\mat{J}^T\mat{J}]_{\alpha \beta}.
\end{gather*}
Note that the transpose is not the same as the dual:
\begin{gather*}
  \bra{X_\alpha} = \ket{X_\alpha}^T \neq \bra{X^\alpha} = \op{g}\bigl(\ket{X_\alpha}, \cdot\bigr).
\end{gather*}
As this suggests, the **duals** are formed using the metric:
\begin{gather*}
  \bra{X_\alpha} = g_{\alpha\beta}\bra{X^\beta}, \qquad \bra{X^\alpha} = g^{\alpha\beta}\bra{X_\beta},\\
  \braket{X^\alpha|X_\beta} = g^{\alpha \gamma}\braket{X_\gamma|X_\beta} = g^{\alpha \gamma}g_{\gamma\beta} = \delta^{\gamma}_{\beta}.
\end{gather*}

{cite}`Arfken:2013` defines
\begin{gather*}
  \ket{X^\alpha} = [\mat{J}^{-1}]^{\alpha}{}_{i}\ket{\hat{e}_i}.
\end{gather*}
This is equivalent to our notation:
\begin{gather*}
  \ket{X^\alpha} = \ket{X_\beta}g^{\beta\alpha} 
  = \ket{\hat{e}_i}[\underbrace{\mat{J}(\mat{J}^{T}\mat{J})^{-1}}_{(\mat{J}^{-1})^{T}}]_{i}{}^{\alpha}.
\end{gather*}
:::
:::{admonition} Concrete Examples: Polar coordinates and a unit sphere
:class: dropdown

* **Polar coordinates**:

  \begin{align*}
    \mat{g} &= \mat{J}^T\mat{J} = 
    \begin{pmatrix}
      \cos\phi & \sin\phi \\
      -r\sin\phi & r\cos\phi
    \end{pmatrix}
    \begin{pmatrix}
      \cos\phi & -r\sin\phi \\
      \sin\phi & r\cos\phi
    \end{pmatrix}\\
    &=
    \begin{pmatrix}
      \cos^2\phi + \sin^2\phi & 0 \\
      0 & r^2(\cos^2\phi + \sin^2\phi)
    \end{pmatrix}\\
    &=
    \begin{pmatrix}
      1 & 0 \\
      0 & r^2
    \end{pmatrix}.
  \end{align*}
  
* **Unit sphere**:

  \begin{align*}
    \mat{g} &= \mat{J}^T\mat{J} \\
    &= 
    \begin{pmatrix}
      \cos\theta\cos\phi &  \cos\theta \sin\phi & -\sin\theta\\
      -\sin\theta\sin\phi  & \sin\theta\cos\phi & 0
    \end{pmatrix}
    \begin{pmatrix}
      \cos\theta\cos\phi & -\sin\theta\sin\phi \\
      \cos\theta \sin\phi & \sin\theta\cos\phi \\
      -\sin\theta & 0
    \end{pmatrix}\\
    &= 
    \begin{pmatrix}
      \cos^2\theta\cos^2\phi +  \cos^2\theta \sin^2\phi +\sin^2\theta & 0\\
      0 & \sin^2\theta\sin^2\phi + \sin^2\theta\cos^2\phi
    \end{pmatrix}\\
    &= \begin{pmatrix}
      1 & 0\\
      0 & \sin^2\theta
    \end{pmatrix}.
  \end{align*}
:::

## The Metric
How do we relate the metric tensor $\op{g}$ with the idea of a metric that allows us to
compute distances?  This is expressed in terms of the **line element** $\d{s}$:
\begin{gather*}
  \d{s}^2 = g_{\alpha\beta}\d{X}^{\alpha}\d{X}^{\beta}
\end{gather*}

:::{admonition} Concrete Examples: Polar coordinates and a unit sphere
:class: dropdown

* **Polar coordinates**:

  \begin{gather*}
    \d{s}^2 = \d{x}^2 + \d{y}^2 = \d{r}^2 + r^2\;\d{\phi}^2.
  \end{gather*}
  
* **Unit sphere**:

  \begin{gather*}
    \d{s}^2 = \d{\theta}^2 + \sin^2\theta\;\d{\phi}^2.
  \end{gather*}
:::

## Derivatives
Complications arrive in the notion of derivatives because, by their nature, they compare
the behavior of vectors in **different spaces** -- at neighboring points.  In the
Euclidean embedded space, this is not an issue since all vectors live in the same space,
but once we introduce coordinate transforms, there is no guarantee about how the tangent
space at neighboring points are related.

:::{important}
The idea of covariance is we consider only quantities that are well defined within the
manifold, or embedding space.  For example, we can consider quantities like the number
returned by a form acting on a vector $\braket{u|v}$ or the number returned by the
metric $\op{g}\bigl(\ket{u}, \ket{v}\bigr)$ (which is the same if the space has a metric
to related $\ket{u}$ and $\bra{u}$.)  These values should not change if we change our
coordinate system: in other words, they are **invariant** under coordinate
transformations.

In contrast, the "numbers" like $V^{\alpha}$, which are the components of the vector in
a particular chart, depend on the choice of coordinates -- specifically through the
Jacobian matrix $J_{\alpha}{}^{i} = \partial_{\alpha}x^{i}$.  Since physicists like to
work with numbers, much attention is given to these coordinates, and **how they
transform** -- so much so that some books refer to **the numbers themselves** as
tensors.  The emphasis on how the components transform is so that physicists can ensure
that they are forming invariant quantities by contracting indices in
covariant-contravariant pairs: $\braket{u|v} = U_{\alpha}V^{\alpha}$.

To ensure that you are studying physics rather than numerology, I recommend that you try
to express all of your final results in terms of fundamental properties of the manifold
-- or at least in terms of properties computed in the embedding space.  Of course, work
with coordinates because they are useful, but don't trust that something is coordinate
invariant just because the indices are properly paired -- make sure you understand what
the quantity means physically.
:::

To see how one might get into difficulty, consider a function $f(x)$ on the manifold
where $x(X)$ depend on some chart coordinates $X$.  The gradient in the coordinate chart is
\begin{gather*}
  \pdiff{f}{X^{\alpha}} = \pdiff{f}{x^{i}}\pdiff{x^{i}}{X^{\alpha}} 
  = \pdiff{f}{x^{i}}J^{i}{}_{\alpha}.
\end{gather*}
We thus see that that coefficients of the gradient behave naturally as a **covector**
\begin{gather*}
  \pdiff{f}{x^{i}} = \pdiff{f}{X^{\alpha}}[\mat{J}^{-1}]^{\alpha}{}_{i}.
\end{gather*}
:::{margin}
In one's work, to minimize writing, the following comma notation is sometimes helpful:
\begin{gather*}
  \pdiff{\psi}{X^{\alpha}} = \partial_{\alpha}\psi = \psi_{,\alpha},\\
  \pdiff{V^{\alpha}}{X^{\beta}} = \partial_{\beta}V^\alpha = V^{\alpha}_{,\beta}.
\end{gather*}
We will not use the latter since, as we shall soon see, $V^{\alpha}_{,\beta}$ are not the
components of a tensor.  This can be useful when working, however.
:::
:::{admonition} Notation
This motivates the following notation to emphasize the covariant nature of derivatives.
\begin{gather*}
  \pdiff{}{X^{\alpha}} = \partial_{\alpha}, \qquad
  \pdiff{}{X_{\alpha}} = \partial^{\alpha} = g^{\alpha\beta}\partial_{\beta}.
\end{gather*}
We will use this notation going forward: 
:::
:::{margin}
This is just the standard differential operator $\op{\d} = \bra{\nabla}$ from
differential geometry.
:::
We can now consider various derivative operations constructed from
\begin{gather*}
  \bra{\nabla} = \bra{X^{\alpha}}\partial_{\alpha}, \qquad
  \ket{\nabla} = \ket{X_{\alpha}}\partial^{\alpha}.
\end{gather*}
Unless specified otherwise, the derivative should act to the right.
:::::{warning}
If you carefully use $\bra{\nabla}$ and $\ket{\nabla}$ as we do below, you will obtain
properly invariant quantities, but you can run into difficulty if you blindly use
$\partial_{\alpha}$.  For example, you might think that
\begin{gather*}
  \partial_{\alpha}V^{\beta}
\end{gather*}
are the components of a tensor in $\mathcal{T}^{1}_{1}$, but it is not.  In particular,
\begin{gather*}
  \partial_{\alpha}V^{\alpha} \neq \braket{\nabla|v}
\end{gather*}
is not the proper divergence in general, and is not invariant under coordinate
transforms.

:::{admonition} Example in Polar Coordinates
:class: dropdown
Consider the vector field $\ket{v} = \ket{x}x + \ket{y}y$ with divergence
$\braket{\nabla|v} = 1$.  In polar coordinates $x+\I y = r^{\I\phi}$ we have
\begin{gather*}
  %\ket{r} = \ket{x}\cos\phi + \key{y}\sin\phi, \qquad
  %\ket{\phi} = -\ket{x}r\sin\phi + \ket{y}r\cos\phi,\\
  \ket{x} = \ket{r}\cos\phi + \ket{\phi}r^{-1}\cos\phi,\\
  \ket{y} = \ket{r}\sin\phi -\ket{\phi}r^{-1}\sin\phi,\\
  \ket{v} = \ket{r}r + \ket{\phi}\cos 2\phi
\end{gather*}
hence
\begin{gather*}
  \partial_{\alpha}V^{\alpha} = \pdiff{r}{r} + \pdiff{\cos 2\phi}{\phi}
  = 1 - 2\sin 2\phi.
\end{gather*}
This only gives the correct answer along the $x$-axis.
:::

:::::

## Christoffel Symbols
:::{margin}
Here is an example of using the "unnatural" 
\begin{gather*}
  \bra{X_{\alpha}} = \pdiff{x^{i}}{X^{\alpha}}\bra{\hat{e}_i}
\end{gather*}
which facilitates computations.
:::
In general, the basis vectors $\ket{X_j}$ for the chart might change as one moves along
the manifold.  This change can re-expressed in terms of the basis:
\begin{gather*}
  \partial_{\beta}\ket{X_\alpha} = \ket{X_\gamma}\Gamma^{\gamma}_{\alpha\beta}, \qquad
  \Gamma^{\gamma}_{\alpha\beta} = \bra{X^\gamma}\partial_{\beta}\ket{X_\alpha}
                  = g^{\gamma\delta}\underbrace{\bra{X_{\delta}}
                    \partial_{\beta}\ket{X_\alpha}}_{[\alpha\beta,\delta]}.
\end{gather*}
:::{margin}
Sometimes you will see the notation
\begin{gather*}
  \Gamma_{\gamma\;\alpha \beta} = g_{\gamma\delta}\Gamma^{\delta}_{\alpha \beta} 
  = [\alpha \beta, \gamma],
\end{gather*}
but don't let this fool you into thinking that $\Gamma$ is a tensor: it is not!
:::
The coefficient $[\alpha\beta,\gamma]$ are known as the **Christoffel symbols of the
first kind** and $\Gamma^{\gamma}_{\alpha\beta}$ are known as the **Christoffel symbols
of the second kind**.  They can be computed by differentiating the basis vectors:
\begin{gather*}
  \partial_{\alpha}g_{\beta \gamma} + \partial_{\beta}g_{\alpha \gamma} 
  - \partial_{\gamma}g_{\alpha\beta} = 2[\alpha \beta, \gamma],\\
  [\alpha \beta, \gamma] = \frac{1}{2}\left(
    \partial_{\alpha}g_{\beta\gamma} + \partial_{\beta}g_{\alpha\gamma}
    - \partial_{\gamma} g_{\alpha\beta}
  \right),\\
  \Gamma^{\gamma}_{\alpha\beta} = \frac{g^{\gamma\delta}}{2}\left(
    \partial_{\alpha}g_{\beta\delta} + \partial_{\beta}g_{\alpha\delta}
    - \partial_{\delta}g_{\alpha\beta}
  \right).
\end{gather*}
These formulae should make it clear that, for the tangent space,
$\Gamma^{\gamma}_{\alpha\beta} = \Gamma^{\gamma}_{\beta\alpha}$ is symmetric in the
lower indices.

:::{important}
Although we use index notation, $\Gamma$ is **not a tensor**.  To see this, note that in
Euclidean coordinates, $g_{ij} = \delta_{ij}$ is constant, so
$\Gamma^{\gamma}_{\alpha\beta} = 0$.  If this were to transform as a tensor by
multipliction with Jacobian factors, then it would have to be zero in all frames, which
is clearly not.
:::
:::{margin}
Hint: Express everything in terms of the coordinate transforms
\begin{gather*}
  J^{i}{}_{j} = \pdiff{x^i}{X_k}
\end{gather*}
and then differentiate.
:::
:::{admonition} Exercise: Compute the Christoffel symbols.
:class: dropdown
These are obtained by differentiating the basis vectors:
\begin{gather*}
  \ket{X_{\alpha}} = \ket{\hat{e}_i}J^{i}{}_{\alpha} 
            = \ket{\hat{e}_i}\pdiff{x^i}{X^{\alpha}},\qquad
  \bra{X^{\alpha}} = \pdiff{x^i}{X_{\alpha}}\bra{\hat{e}_i},\qquad
  \pdiff{\ket{X_{\alpha}}}{X^{\beta}} 
  = \ket{\hat{e}_i}\frac{\partial^2 x^i}{\partial X^{\alpha} \partial X^{\beta}},\\
  [\alpha\beta,\gamma] = \braket{X_{\gamma}|\hat{e}_i}\frac{\partial^2 x^i}{\partial X^{\alpha} \partial X^{\beta}}
  = \pdiff{x^i}{X^{\gamma}}\frac{\partial^2 x^i}{\partial X^{\alpha} \partial X^{\beta}}.
\end{gather*}
Notice that these are symmetric in $\alpha$ and $\beta$:
\begin{gather*}
  [\alpha\beta,\gamma] = [\beta\alpha, \gamma], \qquad
  \Gamma^{\gamma}_{\beta\alpha} = \Gamma^{\gamma}_{\alpha\beta}.
\end{gather*}
Can we express this in terms of the metric?
\begin{gather*}
  g_{\alpha\beta} = [\mat{J}^T\mat{J}]_{\alpha\beta} = J^{i}{}_{\alpha}J^{i}{}_{\beta}
  = \pdiff{x^{i}}{X^{\alpha}}\pdiff{x^{i}}{X^{\beta}},\\
  \newcommand{\dgdq}[4][i]{
    \pdiff{g_{{#2}{#3}}}{X_{#4}} = 
    \pdiff{x^{#1}}{X^{#2}}\frac{\partial^2 x^{#1}}{\partial X^{#3}\partial X^{#4}}
    +
    \pdiff{x^{#1}}{X^{#3}}\frac{\partial^2 x^{#1}}{\partial X^{#2}\partial X^{#4}}}
  \dgdq[i]{\alpha}{\beta}{\gamma} = [\beta\gamma,\alpha] + [\alpha\gamma,\beta].
\end{gather*}
This looks promising, but we need to cancel some terms.  Consider two other permutations
permutations:
\begin{gather*}
  \pdiff{g_{\alpha\gamma}}{X^{\beta}} = [\alpha\beta,\gamma] + [\beta\gamma,\alpha], \qquad
  \pdiff{g_{\beta\gamma}}{X^{\alpha}} = [\alpha\gamma,\beta] + [\alpha\beta,\gamma].
\end{gather*}
This should make it clear that
\begin{gather*}
  \pdiff{g_{\beta\gamma}}{X^{\alpha}} + \pdiff{g_{\alpha\gamma}}{X^{\beta}} -\pdiff{g_{\alpha\beta}}{X^{\gamma}} = 2[\alpha\beta,\gamma],\\
  [\alpha\beta,\gamma] = \frac{1}{2}\left(
    \pdiff{g_{\beta\gamma}}{X^{\alpha}} + \pdiff{g_{\alpha\gamma}}{X^{\beta}} -\pdiff{g_{\alpha\beta}}{X^{\gamma}}
  \right),\\
  \Gamma^{m}_{\alpha\beta} = \frac{g^{m\gamma}}{2}\left(
    \pdiff{g_{\beta\gamma}}{X^{\alpha}} + \pdiff{g_{\alpha\gamma}}{X^{\beta}} -\pdiff{g_{\alpha\beta}}{X^{\gamma}}
  \right).
\end{gather*}
:::
:::{admonition} Concrete Examples: Polar coordinates and a unit sphere
#:class: dropdown

Since the metric in both cases is simple, we use the metric formulae.  To obtain a
non-zero entry for $[\alpha\beta,\gamma]$ we need one of $\beta=\gamma$,
$\alpha=\gamma$, or $\alpha=\beta$.  We can also use the symmetry
$[\alpha\beta,\gamma]=[\beta\alpha,\gamma]$: 

* **Polar coordinates**:
  
  Since $g_{rr} = 1$ and $g_{\phi\phi} = r^2$, the only non-zero contribution will be from
  \begin{gather*}
    \pdiff{g_{\phi\phi}}{X_r} = \pdiff{r^2}{r} = 2r.
  \end{gather*}
  This requires one of $(\alpha, \beta, \gamma) \in \{(r, \phi, \phi), (\phi, r, \phi),
  (\phi, \phi, r)\}$:
  \begin{gather*}
    [r\phi,\phi] = [\phi r,\phi] = - [\phi\phi,r] = r,\\
    \Gamma^{\phi}_{r\phi} = \Gamma^{\phi}_{\phi r} = \frac{1}{r}, \qquad
    \Gamma^{r}_{\phi\phi} = -r.
  \end{gather*}
  Geometrically this makes sense: If we increase $r$, the basis vectors do not rotate,
  but $\ket{\phi}$ will change length, whereas if we increase $\phi$, they both rotate.
  The basis vectors are
  \begin{align*}
    \partial_{\phi}\ket{r} &= -\ket{\hat{x}}\sin\phi + \ket{\hat{y}}\cos\phi 
                            = \frac{1}{r}\ket{\phi}
                            = \ket{\phi}\Gamma^{\phi}_{r\phi},\\
    \partial_{r}\ket{\phi} &= \frac{1}{r}\ket{\phi}
                            = \ket{\phi}\Gamma^{\phi}_{\phi r}, \\
    \partial_{\phi}\ket{\phi} &= -\ket{\hat{x}}r\cos\phi - \ket{\hat{y}}r\sin\phi 
                               = -r\ket{r}
                               = \ket{r}\Gamma^{r}_{\phi\phi}.
  \end{align*}
  
* **Unit sphere**:

  Here we will compute these directly from the basis vectors: 
  \begin{gather*}
    \ket{\theta} = 
    \ket{\hat{x}}\cos\theta\cos\phi + 
    \ket{\hat{y}}\cos\theta\sin\phi -
    \ket{\hat{z}}\sin\theta,
    \\
    \ket{\phi} =
    -\ket{\hat{x}}\sin\theta\sin\phi +
    \ket{\hat{y}}\sin\theta\cos\phi.
  \end{gather*}
  \begin{align*}
    \partial_{\theta}\ket{\theta} &= 
    -\ket{\hat{x}}\sin\theta\cos\phi - 
    \ket{\hat{y}}\sin\theta\sin\phi -
    \ket{\hat{z}}\cos\theta
    = \cot\theta \ket{\theta} +
    -\ket{\hat{x}}\cos\phi/\sin\theta  
    -\ket{\hat{y}}\sin\phi/\sin\theta 
    ,\\
    \partial_{\phi}\ket{\theta} &= 
    -\ket{\hat{x}}\cos\theta\sin\phi + 
    \ket{\hat{y}}\cos\theta\cos\phi
    = \ket{\phi}\cot\theta = \ket{\phi}\Gamma^{\phi}_{\theta\theta},\\
    \partial_{\theta}\ket{\phi} &=
    -\ket{\hat{x}}\cos\theta\sin\phi +
    \ket{\hat{y}}\cos\theta\cos\phi
    = \ket{\phi}\cot\theta = \ket{\phi}\Gamma^{\phi}_{\phi\theta},\\
    \partial_{\phi}\ket{\phi} &=
    -\ket{\hat{x}}\sin\theta\cos\phi
    -\ket{\hat{y}}\sin\theta\sin\phi,\\
  \end{align*}

  The structure is the same, but now we have
  \begin{gather*}
    \pdiff{g_{22}}{X_1} = \pdiff{\sin^2\theta}{\theta} = 2\sin\theta \cos \theta = \sin(2\theta).
  \end{gather*}
  \begin{gather*}
    [12,2] = [21,2] = - [22,1] = \frac{\sin(2\theta)}{2},\\
    \Gamma^{2}_{12} = \Gamma^{2}_{21} = \frac{\sin(2\theta)}{2\sin^2\theta} = \cot\theta, \qquad
    \Gamma^{1}_{22} = -\frac{\sin(2\theta)}{2}.
  \end{gather*}
  Stated more nicely:
  \begin{gather*}
    [\theta\phi,\phi] = [\phi\theta,\phi] = - [\phi\phi,\theta] = \sin\theta\cos\theta,\\
    \Gamma^{\phi}_{\theta\phi} = \Gamma^{\phi}_{\phi \theta} = \cot\theta, \qquad
    \Gamma^{\theta}_{\phi\phi} = -\sin\theta\cos\theta
  \end{gather*}
  
  We can check by direct differentiation:
  \begin{gather*}
    (x, y, z) = (\sin\theta\cos\phi, \sin\theta\sin\phi, \cos\theta)\\
    [\theta\phi,\phi] = \pdiff{x^i}{\phi}\frac{\partial^2 x^i}{\partial\phi\partial\theta}
    = \pdiff{x}{\phi}\frac{\partial^2 x}{\partial\phi\partial\theta} +
      \pdiff{y}{\phi}\frac{\partial^2 y}{\partial\phi\partial\theta}\\
    = -\sin\theta\sin\phi(-\cos\theta\sin\phi) +  \sin\theta\cos\phi(\cos\theta\cos\phi)
    = \sin\theta\cos\theta,\\
    [\phi\phi,\theta] = \pdiff{x^i}{\theta}\pdiff[2]{x^i}{\phi}
    = \pdiff{x}{\theta}\pdiff[2]{x}{\phi} + \pdiff{y}{\theta}\pdiff[2]{y}{\phi}\\
    = \cos\theta\cos\phi(-\sin\theta\cos\phi) +
      \cos\theta\sin\phi(-\sin\theta\sin\phi)
    = -\sin\theta\cos\theta.
  \end{gather*}
:::

## Geodesics

The geodesic equations can be found by minimizing the energy functional 
\begin{gather*}
  E(\gamma) = \frac{1}{2}\int_{a}^{b} \norm{\dot{\gamma}}_g^2\d{t}
\end{gather*}
where $\gamma(t)$ is a path from $a$ and $b$ whose parameterization is chosen so that
the speed is constant over the whole path:
\begin{gather*}
  \norm{\dot{\gamma}}_{g} = \frac{L(\gamma)}{b-a}, \qquad
  L(\gamma) = \int_{a}^{b} \norm{\dot{\gamma}}_{g} \d{t},
\end{gather*}
where $L(\gamma)$ is the length of the path. Recall that the path is described by the coordinates $X^i(t)$, so
\begin{gather*}
  \dot{\gamma} = \ket{X^{i}}\diff{X^i}{t}, \qquad
  \norm{\dot{\gamma}}_{g}^2 = g_{ij}\diff{X^i}{t}\diff{X^j}{t}.
\end{gather*}
The energy can be minimized using calculus of variations, and which gives the **geodesic
equation** 
\begin{gather*}
  \diff[2]{X^k}{t} + \Gamma^{k}_{ij}\diff{X^i}{t}\diff{X^j}{t} = 0.
\end{gather*}
The same Christoffel symbols (of the second kind) enter.

### Gradient
The **gradient** of a function is straightforward:
\begin{gather*}
  \bra{\nabla}\psi = (\partial_{\alpha}\psi)\bra{X^{\alpha}}.
\end{gather*}
Note that this is naturally a form (covector) since the gradient of a function takes a
vector (curve) and computes the directional derivative of the function along that
direction:
\begin{gather*}
  \ket{v} = \ket{X_{\beta}}\dot{X}^{\beta}, \\
    \bigl(\bra{\nabla}\psi\bigr)\ket{v} 
    = (\partial_{\alpha}\psi)
      \underbrace{\braket{X^{\alpha}|X_{\beta}}}_{\delta^{\alpha}_{\beta}}
      \dot{X}^{\beta}
    = \dot{X}^{\alpha}\partial_{\alpha}\psi
    = \nabla_{\ket{v}}\psi.
\end{gather*}
Furthermore, note that, for this to be invariant, the gradient must act as a **covariant
tensor**, hence justifying the compact notation with a **lower index**.

### Covariant Derivative
Differentiating a vector $\ket{v} = \ket{X_\alpha}V^{\alpha}$ is more complicated
because the basis elements may also change, as described by the Christoffel symbols.  We
start by considering the following, where the where the derivative acts to the left:
\begin{gather*}
  \ket{v}\bra{\overset{\leftharpoonup}{\nabla}}
  = \partial_{\alpha}(\ket{X_{\beta}}V^{\beta})\bra{X^{\alpha}}
  = \bigl(
    V^{\beta}\partial_{\alpha}\ket{X_{\beta}}
    +
    \ket{X_{\beta}}\partial_{\alpha}V^{\beta}\bigr)\bra{X^{\alpha}}.
\end{gather*}
Inserting the identity $\op{1} = \ket{X_\alpha}\bra{X^{\alpha}}$ we recover the
Christoffel symbols:
\begin{gather*}
  \ket{v}\bra{\overset{\leftharpoonup}{\nabla}} 
  = \bigl(
    V^{\beta}\ket{X_\gamma}\underbrace{\braket{X^{\gamma}|\partial_{\alpha}|X_{\beta}}}
                                     _{\Gamma^{\gamma}_{\beta\alpha}}
    +
   \ket{X_{\beta}}\partial_{\alpha}V^{\beta}\bigr)\bra{X^{\alpha}}\\
   = \ket{X_\gamma}
   \underbrace{\bigl(
     \partial_{\alpha}V^{\gamma} + V^{\beta}\Gamma^{\gamma}_{\beta\alpha}\bigr)
   }_{V^{\gamma}_{;\alpha}}\bra{X^{\alpha}}.
\end{gather*}
This is the **covariant derivative** of a vector:
\begin{gather*}
  V^{\gamma}_{;\alpha} = \partial_{\alpha}V^{\gamma} + V^{\beta}\Gamma^{\gamma}_{\beta\alpha}.
\end{gather*}
Unlike $\partial_{\beta}V^{\alpha} \equiv V^{\alpha}_{,\beta}$, this transforms properly
and deserves the shorthand notation.

### Divergence
We can also act the form on the vector to obtain the **divergence**:
\begin{align*}
  \braket{\nabla|v} &= \bra{X^{\beta}}\partial_{\beta}(\ket{X_{\alpha}}V^{\alpha})
  = \underbrace{(\braket{X^{\beta}|\partial_{\beta}|X_{\alpha}})}
              _{\Gamma^{\beta}_{\alpha\beta}}V^{\alpha} +
  \underbrace{\braket{X^{\beta}|X_{\alpha}}}
            _{\delta^{\alpha}_{\beta}}
  \partial_{\beta}V^{\alpha}\\
  &= \partial_{\alpha}V^{\alpha} + \Gamma^{{\beta}}_{\beta\alpha}V^{\alpha} 
   = V^{\alpha}_{;\alpha}.
\end{align*}

:::{margin}
Still need to find a nice way of showing this.
:::
The contraction on the two indices of the Christoffel symbol has a nice form in terms of
the determinant of the metric
\begin{gather*}
   \Gamma^{\beta}_{\beta\alpha} = \frac{1}{\sqrt{\det(g)}}\pdiff{\sqrt{\det(g)}}{X^{\alpha}}.
\end{gather*}
:::{margin}
\begin{gather*}
  \det(g) = \varepsilon^{\alpha_1 \cdots \alpha_m}
            g_{1\alpha_1}\cdots g_{m\alpha_m}\\
  = \frac{\varepsilon^{\alpha_1 \cdots \alpha_m}
          \varepsilon^{\beta_1 \cdots \beta_m}
          g_{\alpha_1\beta_1}\cdots g_{\alpha_m\beta_m}}{m!}.
\end{gather*}
:::
where $\det(g)$ is the determinant of $g_{\alpha\beta}$:
\begin{gather*}
  \braket{\nabla|v} = \frac{\partial_{\alpha}\left(\sqrt{\det(g)} V^{\alpha}\right)}
                           {\sqrt{\det(g)}}.
\end{gather*}
:::{margin}
Recall that if your basis is orthogonal, then $g_{\alpha\beta}$ is diagonal, and $\det(g)$ is
simplify the product of the diagonals.
:::

(sec:Laplacian)=
### Laplacian
Finally, the **Laplacian** can be computed by taking the divergence of the gradient
after first using inverse metric to convert the gradient to a vector:
\begin{gather*}
  \bra{\nabla}\psi = \bra{\nabla\psi} = (\partial_{\alpha}\psi)\bra{X^{\alpha}},\\
  \ket{\nabla\psi} = \ket{X_{\alpha}}\partial^{\alpha}\psi 
                   = \ket{X_{\alpha}}g^{\alpha\beta}\partial_{\beta}\psi\\
  \nabla^2\psi = \braket{\nabla|\nabla\psi}
               =
               \frac{\partial_{\alpha}\left(\sqrt{\det(g)}\;\partial^{\alpha}\psi\right)}
                    {\sqrt{\det(g)}}.
\end{gather*}
Expanded fully, this gives:
\begin{gather*}
  \nabla^2\psi
  = \frac{\partial_{\alpha}\left(\sqrt{\det(g)}\;g^{\alpha\beta}\partial_{\beta}\psi\right)}
         {\sqrt{\det(g)}}.
\end{gather*}


[manifold]: <https://en.wikipedia.org/wiki/Differentiable_manifold>
[Whitney embedding theorem]: <https://en.wikipedia.org/wiki/Whitney_embedding_theorem>
[Jacobian matrix]: <https://en.wikipedia.org/wiki/Jacobian_matrix_and_determinant>
[Riemannian manifolds]: <https://en.wikipedia.org/wiki/Riemannian_manifold>
[inner product]: <https://en.wikipedia.org/wiki/Inner_product_space>


