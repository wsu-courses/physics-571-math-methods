(sec:syllabus)=
# Syllabus: Physics 571
## Mathematical Methods for Physics

## Course Information

- **Instructor(s):** Michael McNeil Forbes [`m.forbes+571@wsu.edu`](mailto:m.forbes+571@wsu.edu)
- **Course Assistants:** 
- **Office:** Webster 947F
- **Office Hours:** TBD
- **Course Page:** <https://schedules.wsu.edu/sectionInfo/&campus=Pullman&prefix=Phys&term=Fall&year=2024&course=571&section=1>
- **Class Number:** 571
- **Title:** Phys 571: Mathematical Methods for Physics
- **Credits:** 3
- **Recommended Preparation**: Linear algebra, differential equations, complex
  analysis. Methods will be motivated by physical applications, so general exposure to
  the core concepts of classical mechanics, electromagnetism, quantum mechanics, and
  statistical mechanics would be helpful, i.e. at the level of our undergraduate Modern
  Physics 2 course.  Basic numerical programming techniques (i.e. Python, NumPy, SciPy,
  and Matplotlib). 
  
- **Meeting Time and Location:** MWF, 9:00am - 10:00am, [Webster 941](https://sah-archipedia.org/buildings/WA-01-075-0008-13), Washington State University (WSU), Pullman, WA
- **Grading:** Grade based on assignments and project presentation.

```{contents}
```

### Prerequisites
<!-- Only required for undergraduate courses, but is useful for graduate courses -->

:::{margin}
**Theory track.**
:::
Background will be assumed with the foundations of linear algebra, differential
equations, and complex analysis.  Students interested in the theory track (i.e. those
interested in theoretical or mathematical physics) would also benefit significantly from
some formal mathematics background -- set theory, point-set topology, group theory, analysis,
differential geometry etc.

Methods will be motivated by physical applications, so general exposure to
the core concepts of classical mechanics, electromagnetism, quantum mechanics, and
statistical mechanics would be helpful, i.e. at the level of our undergraduate Modern
Physics 2 course.

Students will be expected to check their work numerically, so familiarity with a
language like Python and the NumPy and SciPy would be helpful. Instructions on how to
use the online [CoCalc][] computational platform will be provided, so students need to
provide their own software, compilers, etc.

### Textbooks and Resources

<table><tr>
  <td><img alt="Arfken cover" 
    src="https://ars.els-cdn.com/content/image/3-s2.0-C20090306297-cov200h.gif"
    height = 300px></td>
  <td><img alt="Hassani cover" 
    src="https://media.springernature.com/w316/springer-static/cover-hires/book/978-3-319-01195-0?as=webp"
    height = 300px></td>
  <td><img alt="Boas cover"
    src="https://media.wiley.com/product_data/coverImage300/69/04711982/0471198269.jpg"
    height = 300px></td>
</tr></table>
    
#### Required
:::{margin}
Available for [online access][Arfken:2013 (ScienceDirect Online)] through the
[WSU Library][Arfken:2013 (WSU Library)] (sign in with your WSU account).
:::
<!-- This must be on one line -->
[Arfken et al., 2013: "Mathematical Methods for Physicists: A Comprehensive Guide"][Arfken:2013 (Elsevier)] 
: This is the primary textbook for the course.  It has good coverage and will be a good
  reference for future work, but lacks mathematical sophistication (i.e. few proofs or
  discussion of formal structures).
  

#### Additional Resources
:::{margin}
Available for [online access][Hassani:2013 (ProQuest Online)] through the [WSU
Library][Hassani:2013 (WSU Library)] (sign in with your WSU account).
:::
<!-- This must be on one line -->
[Hassani, 2013: "Mathematical Physics: A Modern Introduction to Its Foundations"][Hassani:2013 (Springer)]
: This is a supplemental resource for those students interested in pursuing theoretical
  or mathematical physics.  It provides a much more rigorous background into the
  mathematical methods discussed in {cite}`Arfken:2013`, but might be hard to digest for
  students without sufficient mathematical preparation (formal proofs etc.)
  All students are highly encouraged to try to assimilate as much as possible from this
  resource, but this is not a requirement for success in the course.

:::{margin}
Unfortunately, {cite}`Mathews:1970` is not available for online access through the WSU
Library, but a [hard copy][Mathews:1970 (WSU Library)] is available.  You can access it
online through the [internet archive][Mathews:1970 (Internet Archive)].
:::
[Mathews and Walker 1970: "Mathematical Methods of Physics"][Mathews:1970 (WSU Library)]
: This is an older book, but some topics are presented very well and use it extensively
  to prepare my notes.  It is quite terse, and therefore short.  This is maybe not so
  great for learning the first time, but very good for quickly reviewing the important
  points.

:::{margin}
Unfortunately, {cite}`Boas:2006` is not available for online access through the WSU
Library.  A [hard copy][Boas:2006 (WSU Library)] of the 2nd edition is available.
:::
<!-- This must be on one line -->
[Boas 2016: "Mathematical Methods in the Physical Sciences"][Boas:2006 (Wiley)]
: Another standard textbook for this topic that is somewhat easier to read, but not
  quite as comprehensive.  Students who have difficulty reading {cite}`Arfken:2013`
  might find this easier.


:::{margin}
Unfortunately, {cite}`Graham:1994` is not available for online access through the WSU
Library.  A [hard copy][Graham 1994 (WSU Library)] of the 2nd edition is available.
:::
[Graham 1994: "Concrete Mathematics"][Graham:1994]
: This book {cite}`Graham:1994` is deep and the place to go if you need exact results
  about sequences.  It develops a full analog to calculus but for finite difference
  operators, allowing one to "differentiate" and "integrate" series exactly.  I highly
  recommend reading at least the first chapter for insights into how one might approach
  problems of this type with a combination of guess and check, exploration, and finally
  formal proof.

:::{margin}
Available for [online access][Bornemann:2004 (SIAM Online)] through the [WSU
Library][Bornemann:2004 (WSU Library)] (sign in with your WSU account).
:::
<!-- This must be on one line -->
[Bornemann 2004: "The SIAM 100-digit challenge: a study in high-accuracy numerical computing"][Bornemann:2004 (SIAM)]
: This is an excellent and fun book describing methods for solving problems posed in
  [Nick Trefethen's 100-digit
  Challenge](https://en.wikipedia.org/wiki/Hundred-dollar,_Hundred-digit_Challenge_problems).
  It is where I learned about sequence acceleration techniques like [Levin's
  $u$-transformation][Levin transformation], which I often use when quickly trying to
  solve a very hard sequence.

[Levin transformation]: <https://math.stackexchange.com/questions/6625/levins-u-transformation>
[Arfken:2013 (Elsevier)]: 
  <https://doi.org/10.1016/C2009-0-30629-7>
[Arfken:2013 (WSU Library)]: 
  <https://searchit.libraries.wsu.edu/permalink/01ALLIANCE_WSU/1rq08rk/alma99901099339901842>
[Arfken:2013 (ScienceDirect Online)]:
  <https://www.sciencedirect.com/book/9780123846549/mathematical-methods-for-physicists>

[Hassani:2013 (Springer)]:
  <http://dx.doi.org/10.1007/978-3-319-01195-0>
[Hassani:2013 (WSU Library)]:
  <https://searchit.libraries.wsu.edu/permalink/01ALLIANCE_WSU/1rq08rk/alma99901099339801842>
[Hassani:2013 (ProQuest Online)]:
  <https://ebookcentral.proquest.com/lib/wsu/detail.action?docID=6313223>

[Graham:1994]: <https://www-cs-faculty.stanford.edu/~knuth/gkp.html>
[Graham 1994 (WSU Library)]: <https://searchit.libraries.wsu.edu/permalink/01ALLIANCE_WSU/1rq08rk/alma99123631940101842>

[Mathews:1970 (WSU Library)]:
  <https://searchit.libraries.wsu.edu/permalink/01ALLIANCE_WSU/1rq08rk/alma99140432080101842>
[Mathews:1970 (Internet Archive)]:
  <https://archive.org/details/mathematicalmeth0000jonm>
  
[Boas:2006 (Wiley)]: 
  <https://www.wiley.com/en-us/Mathematical+Methods+in+the+Physical+Sciences%2C+3rd+Edition-p-9781118048887>
[Boas:2006 (WSU Library)]: 
  <https://searchit.libraries.wsu.edu/permalink/01ALLIANCE_WSU/1rq08rk/alma99424403480101842>

[Bornemann:2004 (SIAM)]:
  <https://epubs.siam.org/doi/book/10.1137/1.9780898717969>
[Bornemann:2004 (WSU Library)]:
  <https://searchit.libraries.wsu.edu/permalink/01ALLIANCE_WSU/1rq08rk/alma99900918324501842>
[Bornemann:2004 (SIAM Online)]:
  <https://epubs-siam-org.eu1.proxy.openathens.net/doi/book/10.1137/1.9780898717969>

### Student Learning Outcomes

By the end of this course, students will:

1. Be aware of the standard techniques of mathematical physics.
2. Be able to quickly apply these techniques to well-posed problems.
3. Be able to formulate such well-posed problems to solve general physics problems.
4. :::{margin}
   **Theory track.**
   :::
   (Theory Track): Understand the mathematical formulation of the techniques and how
   to prove key results.

### Expectations for Student Effort

For each hour of lecture equivalent, all students should expect to have a minimum of two
hours of work outside class.  All students are expected to keep up with the readings
assigned in class, ask questions in class and through the Perusall/Hypothes.is forums,
and complete homework on time.

:::::{admonition} Important! A message from the instructor to the students. 

The path to mastery is personal, and each student must determine what works for them.
**You are ultimately responsible for mastering the material.**  
The department offers a variety of different approaches, including textbooks,
supplemental readings, problems and assignments, online resources, computer
simulations, and interactions with peers, the instructor, and members of the
department. Although there are many paths to mastery, some aspects appear to be
universal:

Struggle (Active Engagement)
: Mastery requires struggling with, and ultimately overcoming, challenges posed
  by new techniques and ideas.  If you are not struggling, you are not learning
  effectively.  Part of the challenge is to identify the right level: If the material
  seems easy, then you are simply reinforcing things you already have learned (important
  for long-term learning); If the material is too challenging, you might not be able to
  make enough progress to engage.

Translation
: The act of translating often plays a key role.  For example, to engage with readings,
  one must translate from the language and presentation style of the author to a form
  that makes sense to you.  Almost certainly, the picture that the author has is
  different than yours.  You have a different background, know different things, and
  possibly think in a different way: e.g. some people think most naturally in terms of
  symbols (algebra), while others think more naturally in terms of pictures (geometry).

  To effectively translate the material, you must **make your own notes**.  This
  translation can occur in many ways.  For example, the I find it extremely valuable to
  translate mathematical ideas into numerical computer codes.  Find out what works for
  you, then do it.
    
Simply put, **learning requires effort**.  To learn, your brain must change, and this
change requires energy.  If you do not expend this energy, you cannot learn.

Students are expected to use the class resources and assessments to determine the limits
of their understanding, and then to expand these boundaries.  Students are expected to
seek help from their classmates, the instructor, and others in the department.  The
department cultivates an environment and community of learning, and students are
strongly encourage to discuss problems with others at events such as the iSciMath coffee
hours Fridays 2-4 in the Band Room - Webster 1243, or by making appointments with the
instructor.

:::{admonition} My approach.
:class: dropdown

Here is how I approach learning new material.  It may or may not work for you, depending
on your learning style.  I highly recommend that you try to formulate a similar approach
for yourself once you figure out what works well.  *(I would be very interested in
seeing your formulations if they differ substantially from mine.)*

1. I usually start by scanning at a high level what it is I am trying to learn.  This
   might be by skimming a textbook, often looking for interesting pictures since I am
   quite geometric in my thinking, or by listening to a talk, or reviewing a video
   online.
2. Once I identify some of the key points, I go and try to derive those points myself.
   How would I approach the problem given my current knowledge and skills?  If I can
   derive the main results, then I usually have an excellent grasp of the material.
3. Often, however, I get stuck.  In this case, I start reading etc. more details to
   figure out what I am missing.  For example, I might find that I need to learn some
   other technique first to make progress, or I might be missing a key insight.  In the
   latter case, the point is that I first tried and failed.  This primes me for
   learning *and remembering* the insight.  Even if I can come up with my own
   derivation, reading the derivation in the book might provide additional insights.
   
   *(Contrast this we me simply reading the derivation the first time.  While reading
   such a derivation, the key insights often appear "obvious" in such a way, that I
   might not even notice them, and certainly will not remember them.  This often leads
   to the phenomena of listening to a "great" talk where the presenter makes everything
   seem logical and clear.  However, upon trying to explain the talk to a friend, I
   start to find that I cannot reproduce the argument -- at certain stages, insight must
   be used to make progress in the right direction, and since I did not struggle, I
   cannot remember.*
4. Next I try testing myself by doing the **Examples** in the book without looking at
   the solution.  If I can solve them, I look at the solution to check (or see if there
   is another strategy).  If I can't solve the problem, I return to step 3.
   
   Note: I treat the solutions to the Examples as valuable ways of checking my
   understanding -- I do not waste them trying to learn the material.
5. Next I turn to the **Exercises**.  If I have learned the material, I am usually able
   to quickly read these and know if I would be able to solve the problem given enough
   time (and make a note of how long I think it would take me.)  If I can't, then I use
   this as another way to go back to step 3 for those things I am missing.
   
   Once I can see my way through all of the exercises quickly, I check that my
   assessment is correct by doing a few to make sure I can actually solve them, and to
   check my estimate for how long it will take me.  This helps me calibrate in the
   future.  If I am missing something, I go back to step 3.
6. I next try to reinforce what I learned/discovered.  The best approach is to **teach
   the material**.  This works best if I can find someone to teach, but there are many
   options:
   1. Write pedagogical notes like these class notes.
   2. Teach a computer.  If you can program a computer to solve your problem (without
      relying on black-box libraries) then you know that you are not missing anything.
   3. Teach your cat/dog.  Even if the subject has no hope of understanding, I find that
      the act of translating the ideas into language helps me secure the knowledge.
7. Finally, I love talking to people when I am stuck.  I try to simplify the problem to
   its essence so I can quickly communicate where I am stuck to colleagues.  On one
   extreme, my colleagues might understand the material, and they can quickly teach me
   because I have put the effort in to be ready to learn.  On the other extreme, my
   colleagues might also be stumped.  In this case, I have likely identified something
   worth publishing once I figure it out.
:::
:::::

### Assessment and Grading Policy

Students will be assessed with weekly assignments to ensure that the content-based
learning outcomes 1 and 2 are realized.  To ensure students have enough practice,
problems will be designed so that students can self-assess using numerical techniques to
check their work.  Students will be expected to self-assess before handing in the
assignments -- those who cannot reconcile the numerical checks with their analytic
work are expected to seek help from the instructor prior to submitting their assignment.

Included in several of these weekly assignments will be larger more abstract physics
problems that required the use of a combination of previous techniques to assess
learning outcome 3.

Students are expected to keep up with the assigned readings.  To encourage students to
do this, at the start of every class, one or more students will be selected at random to
present the solution to one of the Examples or Exercises in the reading.  The
participation portion of the grade will be assigned based on these presentations with
full points awarded if the students demonstrate that they have engaged with the required
reading.  This participation grade does **not** depend a successful answering of the
question (see below).

:::::{admonition} Rational
:class: dropdown

There is a lot of material to be covered -- far too much to be thoroughly presented in
the available class time.  Furthermore, students are arriving with very different
backgrounds.  To ensure mastery, students must engage with the material by reading the
textbook, and by working through problems.

Typically in my courses, I try to structure the exams to mirror the qualifying exams --
a written portion followed by an oral exam.  This works well for more complex problems
in courses like Classical Mechanics, but is not well suited to a course like this where
there are many small techniques to learn, so traditional in-class exams will be used
instead.
   
That being the case, I still want to ensure you have experience answering questions
orally as will be required in the qualifying examinations.  Remember -- you are not
being graded on your solution, only that you have engaged with the material and are
ready to learn in class.  If you have questions about the material, you are encouraged
to ask them during your presentation.

Why not just use the assignments?  Unfortunately, students seem to have the
misconception that finding solutions online and reading these is sufficient
preparation.  Generating truly unique problems that are not too burdensome is extremely
challenging, thus, using online resources is often a good way to get a good assignment
grade without learning the material well.  *(This is cheating, but it is virtually
impossible to prevent/detect.)* In my experience, the best way to check if students have
sufficiently understood the material is to interact with them asking questions, and
given the time limitations of the course, this is the best approach I have found.
:::::


:::{margin}
**Theory track.**
:::
Students in the theory track will be presented with several alternative problems that
emphasize more mathematical approaches, including proofs of key results, assessing
learning outcome 4.

* 10% Participation
* 20% Assignments (Never accepted late)
* 20% First Midterm
* 20% Second Midterm
* 30% Final Exam (Wed 11 December 2024: 8am-10am)

The final grade will be converted to a letter grade using the following scale: 

| Percentage P       | Grade |
| ------------------ | ----- |
| 90.0% ≤ P          | A     |
| 85.0% ≤ P \< 90.0% | A-    |
| 80.0% ≤ P \< 85.0% | B+    |
| 75.0% ≤ P \< 80.0% | B     |
| 70.0% ≤ P \< 75.0% | B-    |
| 65.0% ≤ P \< 70.0% | C+    |
| 60.0% ≤ P \< 65.0% | C     |
| 55.0% ≤ P \< 60.0% | C-    |
| 50.0% ≤ P \< 55.0% | D+    |
| 40.0% ≤ P \< 50.0% | D     |
| P \< 40.0%         | F     |

### Attendance and Make-up Policy 

While there is no strict attendance policy, students are expected attend an participate
in classroom activities and discussion. Students who miss class are expected to cover
the missed material on their own, e.g. by borrowing their classmates notes, reviewing
recorded lectures (if available), etc.

### Course Timeline

Here is a plausible approach to completely reviewing Arfkin in the ~38 lectures we have.
This schedule will be rearranged as needed to match the ability of the students.

| Date          | Arfken     | Hassani | Comments                                      |
|---------------|------------|---------|-----------------------------------------------|
| <s>19 Aug</s> |            |         | *(Qualifying Exams - No class)*               |
| 21 Aug        |            |         |                                               |
| 23 Aug        | 1.1-1.2    |         | Series                                        |
|               |            |         |                                               |
| 26 Aug        | 1.3-1.9    |         | Binomial theorem, Vectors, Derivatives        |
| 28 Aug        | 1.10-2.1   |         | Complex, Integrals, Delta functions           |
| 30 Aug        | 2.2-3.1    |         | Matrices, Vector spaces                       |
|               |            |         |                                               |
|               |            |         |                                               |
| <s>2 Sep</s>  |            |         | *(Labour Day - No class)*                     |
| 4 Sep         | 3.2-3.6    |         | 3D Vectors                                    |
| 6 Sep         | 3.7-3-10   |         | Vector Calculus                               |
|               |            |         |                                               |
| 9 Sep         | 5.1-5.7    |         | Vector spaces.                                |
| 11 Sep        | 6.1-6.5    |         | Eigenvalues                                   |
| 13 Sep        | 3.7-3-10   |         | Vector Calculus                               |
|               |            |         |                                               |
| 13 Sep        | 4.1-4.4    | 26, 28  | Tensors                                       |
| 18 Sep        |            | 36, 37  | Tensors Cont.                                 |
| 20 Sep        |            |         | Tensors Cont.                                 |
|               |            |         |                                               |
| 23 Sep        | 7.1-7.5    | 14      | ODEs                                          |
| 25 Sep        | 7.6-7.8    | 14.5    | Inhomogeneous ODEs                            |
| 27 Sep        | 8.1-8.5, 12.1 | 19, 7.2.1 | Sturm-Liouville Theory, Orthogonal Polynomials |
|               |            |         |                                               |
|               |            |         |                                               |
| 30 Sep        | 9.1-9.3    |         | PDEs                                          |
| 2 Oct         |            |         | No Class (Swap with Classical Mechanics)      |
| 4 Oct        | 9.5-9.8, 10.1-10.2 |  | Double Class: Laplace, Waves, Heat, Green's Functions |
|               |            |         |                                               |
| 7 Oct         |            |         | Midterm I                                     |
| 9 Oct         |            |         | Midterm Review, PDE/Green's Function Review   |
| 11 Oct        | 11.1-11.6  |         | Complex analysis                              |
|               |            |         |                                               |
| 14 Oct        | 11.1-11.6  |         | Complex analysis continued.                   |
| 16 Oct        | 11.7-11.10 |         | Residues                                      |
| 18 Oct        | 17         |         | Group Theory (skim and ask questions)         |
|               |            |         |                                               |
| 21 Oct        | 12.1-12.5  |         | Ortho. Polynomials/$B_n$/Zeta/Asymptotic Series |
| 23 Oct        | 12.6-12.8  |         | Steepest descent.  Dispersion.                |
| 25 Oct        | 13.1-13.6  |         | $\Gamma$ etc. More Functions ($\zeta$)        |
|
| 28 Oct        | 19         |         | Fourier Series                                |
| 30 Oct        | 20.1-20.5  |         | Integral Transforms                           |
| 1  Nov        | 20.6-20.10 |         | Laplace transform.  Bit long.                 |
|               |            |         |                                               |
| 4 Nov         | 21         |         | Integral equations.                           |
| 6 Nov         | 23.1-23.4  |         | Probability and Statistics                    |
| 8 Nov         |            |         | Monte Carlo/Sampling                          |
|               |            |         |                                               |
| <s>11 Nov</s> |            |         | *(Veteran's day - No class)*                  |
| 13 Nov        | 20.6-20.10 |         | Laplace transform.  Bit long.                 |
| 15 Nov        | 21         |         | Integral equations.                           |
|               |            |         |                                               |
| 18 Nov        |            |         | Midterm II  <!-- Napili -->                   |
| 20 Nov        | 22.1-22.3  |         | Calculus of Variations                        |
| 22 Nov        | 22.4       |         | Constraints (short)                           |
|
| 25 Oct        | 16.1-16.4  |         | Angular Momentum *(Too long?)*                |
|               |            |         |                                               |
| <s>25 Nov</s> |            |         | *(Thanksgiving - No class)*                   |
| <s>27 Nov</s> |            |         | *(Thanksgiving - No class)*                   |
| <s>29 Nov</s> |            |         | *(Thanksgiving - No class)*                   |
|
| 28 Oct        | 15.5-15.6  |         | Spherical harmonics (short).                  |
| 30 Oct        | 17.1-17.6  |         | Finite groups.                                |
| 1 Nov         | 17.7-17.10 |         | Continuous groups.                            |
|               |            |         |                                               |
|               |            |         |                                               |
|               |            |         |                                               |
|               |            |         |                                               |
| 2 Dec         | 23.1-23.4  |         | Probability and Statistics *(Dead week)*      |
| 4 Dec         | 23.5-23.7  |         | *(Dead week)*                                 |
| 6 Dec         |            |         | *(Dead week)*                                 |
|               |            |         |                                               |
| 11 Dec        |            |         | Final Exam 8-10am                             |
|               |            |         |                                               |
|               | 4.5-4.7    |         | Differential forms - defer?                   |
|               | 15.1-15.4  |         | Legendre functions.  Likely left as reference |
|               | 18.1-18.4  |         | More Special Functions.                       |
|               | 18.5-18.8  |         |                                               |
|               |            |         |                                               |
|
|               | 14.1-14.3  |         | Bessel functions. Likely used as reference.   |
|               | 14.4-14.7  |         |                                               |
|               |            |         |                                               |
|               | 16.1-16.4  |         | Angular Momentum *(Too long?)*                |
|               |            |         |                                               |
|               | 15.5-15.6  |         | Spherical harmonics (short).                  |
|               | 17.1-17.6  |         | Finite groups.                                |
|               | 17.7-17.10 |         | Continuous groups.                            |


## Other Information

### Policy for the Use of Large Language Models (LLMs) or Generative AI in Physics Courses

The use of LLMs or Generative AI such as Chat-GPT is becoming prevalent, both in
education and in industry.  As such, we believe that it is important for students to
recognize the capabilities and inherent limitations of these tools, and use them
appropriately.

To this end, **please submit 4 examples of your own devising:**
* Two of which demonstrate the phenomena of "hallucination" -- Attempt to use the tool
  to learn something you know to be true, and catch it making plausible sounding
  falsehoods.
* Two of which demonstrate something useful (often the end of a process of debugging and
  correcting the AI).

Note: one can find plenty of examples online of both cases.  Use these to better
understand the capabilities and limitations of the AIs, but for your submission, please
find your own example using things you know to be true. *If you are in multiple courses,
you may submit the same four examples for each class, but are encouraged to tailor your
examples to the course.*

Being able to independently establish the veracity of information returned by a search,
an AI, or indeed any publication, is a critical skill for a scientist.  **If you are the
type of employee who can use tools like ChatGPT to write prose, code etc., but not
accurately validate the results, then you are exactly the type of employee that AI will
be able to replace.** 

Any use of Generative AI or similar tools for submitted work **must include**:
1. **A complete description of the tool.** (E.g. *"ChatGPT Version 3.5 via CoCalc's
   interface"* or *Chat-GPT 4 through Bing AI using the Edge browser"*, etc.)
2. **A complete record of the queries issued and response provided.**  (This should be
   provided as an attachment, appendices, or supplement.)
3. **An attribution statement consistent with the following:**
   *“The author generated this <text/code/etc.> in part with <GPT-3, OpenAI’s
   large-scale language-generation model/etc.> as documented in appendix <1>. Upon
   generating the draft response, the author reviewed, edited, and revised the response
   to their own liking and takes ultimate responsibility for the content.”*
   
Violation of this policy may result in failure of the assignment or course.

<!-- ### COVID-19 Statement -->
<!-- Per the proclamation of Governor Inslee on August 18, 2021, **masks that cover both the -->
<!-- nose and mouth must be worn by all people over the age of five while indoors in public -->
<!-- spaces.**  This includes all WSU owned and operated facilities. The state-wide mask mandate -->
<!-- goes into effect on Monday, August 23, 2021, and will be effective until further -->
<!-- notice.  -->
 
<!-- Public health directives may be adjusted throughout the year to respond to the evolving -->
<!-- COVID-19 pandemic. Directives may include, but are not limited to, compliance with WSU’s -->
<!-- COVID-19 vaccination policy, wearing a cloth face covering, physically distancing, and -->
<!-- sanitizing common-use spaces.  All current COVID-19 related university policies and -->
<!-- public health directives are located at -->
<!-- [https://wsu.edu/covid-19/](https://wsu.edu/covid-19/).  Students who choose not to -->
<!-- comply with these directives may be required to leave the classroom; in egregious or -->
<!-- repetitive cases, student non-compliance may be referred to the Center for Community -->
<!-- Standards for action under the Standards of Conduct for Students. -->

### Academic Integrity

You are responsible for reading WSU’s [Academic Integrity Policy][], which is based on
[Washington State law][]. If you cheat in your work in this class you will: 

* Fail the course.
* Be reported to the [Center for Community Standards][].
* Have the right to appeal the instructor's decision.
* Not be able to drop the course or withdraw from the course until the appeals process
  is finished.

If you have any questions about what you can and cannot do in this course, ask your instructor.

If you want to ask for a change in the instructor's decision about academic integrity,
use the form at the [Center for Community Standards][] website. You must submit this
request within 21 calendar days of the decision.

[Academic Integrity Policy]: 
  <https://communitystandards.wsu.edu/policies-and-reporting/academic-integrity-policy/>
[Washington State law]: <https://apps.leg.wa.gov/wac/default.aspx?cite=504-26-202>
[Center for Community Standards]: <https://communitystandards.wsu.edu/>

<!-- Academic integrity is the cornerstone of higher education.  As such, all members of the -->
<!-- university community share responsibility for maintaining and promoting the principles -->
<!-- of integrity in all activities, including academic integrity and honest -->
<!-- scholarship. Academic integrity will be strongly enforced in this course.  Students who -->
<!-- violate WSU's Academic Integrity Policy (identified in Washington Administrative Code -->
<!-- (WAC) [WAC 504-26-010(4)][wac 504-26-010(4)] and -404) will fail the course, will not -->
<!-- have the option to withdraw from the course pending an appeal, and will be Graduate: -->
<!-- 6300, 26300 reported to the Office of Student Conduct. -->

<!-- Cheating includes, but is not limited to, plagiarism and unauthorized collaboration as -->
<!-- defined in the Standards of Conduct for Students, [WAC 504-26-010(4)][wac -->
<!-- 504-26-010(4)]. You need to read and understand all of the [definitions of -->
<!-- cheating][definitions of cheating].  If you have any questions about what is and is not -->
<!-- allowed in this course, you should ask course instructors before proceeding. -->

<!-- If you wish to appeal a faculty member's decision relating to academic integrity, please -->
<!-- use the form available at \_communitystandards.wsu.edu. Make sure you submit your appeal -->
<!-- within 21 calendar days of the faculty member's decision. -->

<!-- Academic dishonesty, including all forms of cheating, plagiarism, and fabrication, is -->
<!-- prohibited. Violations of the academic standards for the lecture or lab, or the -->
<!-- Washington Administrative Code on academic integrity -->

### University Syllabus
<!-- Required as of Fall 2023 -->

Students are responsible for reading and understanding all university-wide policies and
resources pertaining to all courses (for instance: accommodations, care resources,
policies on discrimination or harassment), which can be found in the [university
syllabus][].

[university syllabus]: <https://syllabus.wsu.edu/university-syllabus/>

### Students with Disabilities

Reasonable accommodations are available for students with a documented
disability. If you have a disability and need accommodations to fully
participate in this class, please either visit or call the Access
Center at (Washington Building 217, Phone: 509-335-3417, E-mail:
<mailto:Access.Center@wsu.edu>, URL: <https://accesscenter.wsu.edu>) to schedule
an appointment with an Access Advisor. All accommodations MUST be
approved through the Access Center. For more information contact a
Disability Specialist on your home campus.

### Campus Safety

Classroom and campus safety are of paramount importance at Washington
State University, and are the shared responsibility of the entire
campus population. WSU urges students to follow the “Alert, Assess,
Act,” protocol for all types of emergencies and the “[Run, Hide, Fight]”
response for an active shooter incident. Remain ALERT (through direct
observation or emergency notification), ASSESS your specific
situation, and ACT in the most appropriate way to assure your own
safety (and the safety of others if you are able).

Please sign up for emergency alerts on your account at MyWSU. For more
information on this subject, campus safety, and related topics, please
view the FBI’s [Run, Hide, Fight] video and visit [the WSU safety
portal][the wsu safety portal].

### Students in Crisis - Pullman Resources 

If you or someone you know is in immediate danger, DIAL 911 FIRST! 

* Student Care Network: https://studentcare.wsu.edu/
* Cougar Transit: 978 267-7233 
* WSU Counseling and Psychological Services (CAPS): 509 335-2159 
* Suicide Prevention Hotline: 800 273-8255 
* Crisis Text Line: Text HOME to 741741 
* WSU Police: 509 335-8548 
* Pullman Police (Non-Emergency): 509 332-2521 
* WSU Office of Civil Rights Compliance & Investigation: 509 335-8288 
* Alternatives to Violence on the Palouse: 877 334-2887 
* Pullman 24-Hour Crisis Line: 509 334-1133 

[communitystandards.wsu.edu]: https://communitystandards.wsu.edu/
[definitions of cheating]: https://apps.leg.wa.gov/WAC/default.aspx?cite=504-26-010
[run, hide, fight]: https://oem.wsu.edu/emergency-procedures/active-shooter/
[the wsu safety portal]: https://oem.wsu.edu/about-us/
[wac 504-26-010(4)]: https://apps.leg.wa.gov/WAC/default.aspx?cite=504-26
[SSH]: <https://en.wikipedia.org/wiki/Secure_Shell> "SSH on Wikipedia"
[CoCalc]: <https://cocalc.com> "CoCalc: Collaborative Calculation and Data Science"
[GitLab]: <https://gitlab.com> "GitLab"
[GitHub]: <https://github.com> "GitHub"
[Git]: <https://git-scm.com> "Git"
[Anki]: <https://apps.ankiweb.net/> "Powerful, intelligent flash cards."


[Official Course Repository]: <https://gitlab.com/wsu-courses/physics-571-math-methods> "Official Course Repository hosted on GitLab"
[Shared CoCalc Project]: <https://cocalc.com/ea43b5b0-4cf7-4127-a191-d920020d88b5/> "Shared CoCalc Project"
[WSU Courses CoCalc project]: <https://cocalc.com/projects/c31d20a3-b0af-4bf7-a951-aa93a64395f6>
