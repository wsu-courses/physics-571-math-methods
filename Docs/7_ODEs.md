---
jupytext:
  formats: ipynb,md:myst,py:light
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
FIG_DIR = Path(mmf_setup.ROOT) / '../Docs/_build/figures/'
os.makedirs(FIG_DIR, exist_ok=True)
import logging; logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
try: from myst_nb import glue
except: glue = None
```

(sec:ODEs)=
# 7. Ordinary Differential Equations (ODEs)

There are a few special cases that are exactly solvable.  Most tricks attempt to bring
an equation into one of these forms.  Note that there are many tricks, and so often it
is easiest to try to use a table of exact results like  {cite}`Polyanin:2003` if you
need an exact solution.  These notes are intended as a summary of Chapter 7 of
{cite}`Arfken:2013`.

## Separable

\begin{gather*}
  P(x)\d{x} + Q(y)\d{y} = 0, \qquad
  \int_{x_0}^{x}P(x)\d{x} = \int_{y_0}^{y}Q(y)\d{y}.
\end{gather*}
For most physics problems, I highly recommend including the endpoints of integration --
this makes determining the constants of integration much more natural.

## Exact

\begin{gather*}
  \underbrace{P(x,y)}_{\partial_x f}\d{x} + \underbrace{Q(x,y)}_{\partial_y f}\d{y} = 0, \qquad
  \partial_{y}\underbrace{P(x,y)}_{\partial_{x}f} =
  \partial_{x}\underbrace{Q(x,y)}_{\partial_{y}f},\\
  f(x,y) = C.
\end{gather*}

## Isobaric/Homogeneous

These are equations of the form
\begin{gather*}
  \diff{x}{y} = f(x,y), \qquad
  f(sx, s^{m}y) = s^{m-1}f(x, y).
\end{gather*}
Basically, this means that the equation makes sense dimensionally, and that one can
introduce a dimensionless variable $v = y/x^{m}$ so $y = vx^{m}$ to separate the equations:
\begin{gather*}
  y' = v'x^{m} + mv x^{m-1} = f(x, vx^m) = x^{m-1}f(1, v)\\
  \frac{\d{v}}{mv - f(1, v)} + \frac{\d{x}}{x} = 0.
\end{gather*}
The basic idea is that $x$ and $y$ have the "same" dimensions (up to a power $m$), and this
just defines the units for the problem.  Once these units are scaled out, we obtain a
separable equation for the non-trivial dimensionless combination $v$.

## Integrating Factors

Linear first-order equations always have a solution
\begin{gather*}
  y' + p(x)y = q(x).
\end{gather*}
As {cite}`Arfken:2013` shows, one can try to find an integrating factor that makes it
exact.  Another option called the [method of undetermined coefficients][] is to solve
the homogeneous equation $y' + py = 0$ to find $y = Ae^{-px}$ then let $A(x)$ depend on
$x$.  Plugging this back into the original equation gives an equation for $A(x)$ that
can be solved.  You should try both to see which is  easiest for you, and then remember
the strategy.

If you have a particular solution to a second order equation $y'' + p(x)y' + q(x)y =
r(x)$, then methods exists for finding the general solution: see {cite}`Polyanin:2003`
for details.

## Variation of Parameters
A related approach is Lagrange's method of [variation of parameters][], which is useful
for finding particular solutions of the inhomogeneous equation
\begin{gather*}
  y'' + p(x)y' + q(x)y = f(x)
\end{gather*}
**if you know the homogeneous solutions**.  I.e., let $y_n(x)$ satisfy the homogeneous
equation
\begin{gather*}
  y'' + p(x)y' + q(x)y = 0,
\end{gather*}
then look for solutions that are linear combinations of the form
\begin{gather*}
  y(x) = u_1(x)y_1(x) + u_2(x)y_2(x).
\end{gather*}
Dropping the explicit $x$ dependence in further expressions, the trick is to impose the
condition $u_1'y_1 + u_2'y_2 = 0$ so that we have the following:
\begin{align*}
  y &= u_1y_1 + u_2y_2,\\
  y' &= u_1y'_1 + u_2y'_2 + \smash{\overbrace{u'_1y_1 + u'_2 y_2}^{0}},\\
  y'' &= \underbrace{u_1y''_1 + u_2y''_2}_{\text{will cancel}} 
       + \underbrace{u_1'y'_1 + u_2'y'_2}_{\text{will remain}}.
\end{align*}
Substituting these back into the original equation, the left set of terms cancel, and
what remains, including the trick, is a solvable linear set of equations for $u'_n(x)$:
:::{margin}
The determinant $W$ that appears in the solution here is called the [Wronskian][].
{cite}`Mathews:1970` points out that, if we take the $u_n' = c_n$ to be coefficients and
evaluate the derivatives at a point $x_0$ then the following provides the "best"
approximation to $y(x)$ we can get at $x_0$ using the functions $y_n(x)$ as a basis:
\begin{gather*}
  y(x) \approx c_1y_1(x) + c_2y_2(x),\\
  \begin{pmatrix}
    y_1(x_0) & y_2(x_0)\\
    y_1'(x_0) & y_2'(x_0)\\
  \end{pmatrix}
  \begin{pmatrix}
  c_1\\
  c_2
  \end{pmatrix}
  =
  \begin{pmatrix}
    y(x_0)\\
    y'(x_0)
  \end{pmatrix}.
\end{gather*}
This is called an [osculating][] (*kissing*) approximation.
:::
\begin{align*}
  u_1'y_1 + u_2'y_2 &= 0,\\
  u_1'y'_1 + u_2'y'_2 &= \frac{f(x)}{p(x)},
  \\
  \begin{pmatrix}
    y_1 & y_2\\
    y'_1 & y'_2
  \end{pmatrix}
  \begin{pmatrix}
    u_1' \\
    u_2'
  \end{pmatrix}
  &=
  \begin{pmatrix}
    0\\
    f(x)/p(x)
  \end{pmatrix},
  \\
  \begin{pmatrix}
    u_1' \\
    u_2'
  \end{pmatrix}
  &=
  \frac{1}{W}
  \begin{pmatrix}
    y'_2 & -y_2\\
    -y'_1 & y_1
  \end{pmatrix}
  \begin{pmatrix}
    0\\
    f(x)/p(x)
  \end{pmatrix},
  \\
  W &= \begin{vmatrix}
    y_1 & y_2\\
    y_1' & y_2'
  \end{vmatrix}
  =y_1y_2' - y_2y_1'.
\end{align*}
Note that this works for higher order equations.










[method of undetermined coefficients]: <https://en.wikipedia.org/wiki/Method_of_undetermined_coefficients>
[delta function]: <https://en.wikipedia.org/wiki/Dirac_delta_function>
[Riemann-Stieltjes Integral]: <https://en.wikipedia.org/wiki/Riemann%E2%80%93Stieltjes_integral>
[Heaviside step function]: <https://en.wikipedia.org/wiki/Heaviside_step_function>
[distribution]: <https://en.wikipedia.org/wiki/Distribution_(mathematics)>
[variation of parameters]: <https://en.wikipedia.org/wiki/Variation_of_parameters>
[Wronskian]: <https://en.wikipedia.org/wiki/Wronskian>
[osculating]: <https://en.wikipedia.org/wiki/Osculating_curve>
