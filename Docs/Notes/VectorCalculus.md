---
jupytext:
  formats: ipynb,md:myst,py:light
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
FIG_DIR = Path(mmf_setup.ROOT) / '../Docs/_build/figures/'
os.makedirs(FIG_DIR, exist_ok=True)
import logging; logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
try: from myst_nb import glue
except: glue = None
```

(sec:VectorCalculus)=
Vector Calculus
===============

:::{margin}
The subtleties including ensuring that $\vect{P}$ is smooth enough ($\mathcal{C}^1$ is
sufficient), and that it behaves well at the boundary of the domain, i.e., falling to
zero sufficiently fast at infinity.
:::
## [Helmholtz Decomposition][]

The essence is that, modulo subtleties, any vector field $\vect{P}$ can be expressed as
the sum of a pure divergence and a pure curl:
\begin{gather*}
  \vect{P} = -\vect{\nabla}\phi + \vect{\nabla}\times \vect{A}.
\end{gather*}
This decomposition is not unique, and is invariant under the following transformation:
\begin{gather*}
  \phi \rightarrow \phi + \delta \phi, \qquad
  \vect{A} \rightarrow \vect{A} + \delta\vect{A} + \vect{\nabla}\varphi
\end{gather*}
where
\begin{gather*}
  \vect{\nabla}\times \delta\vect{A} = \vect{\nabla}\delta\phi,
\end{gather*}
and $\varphi$ is an arbitrary scalar field.  Taking the divergence of this equation
gives
\begin{gather*}
  \nabla^2\delta\phi = 0,
\end{gather*}
so $\delta \phi$ is a harmonic function.  To see that this works, simply effect the transformation
\begin{gather*}
  \delta\vect{P} = -\vect{\nabla}\delta\phi + \vect{\nabla}\times(\delta\vect{A} +
  \vect{\nabla} \varphi)\\
  = \underbrace{-\vect{\nabla}\delta\phi + \vect{\nabla}\times\delta\vect{A}}_{0} +
  \underbrace{\vect{\nabla}\times(\vect{\nabla} \varphi)}_{0}.
\end{gather*}
This decomposition allows one to express $\vect{P}$ in terms of a distribution of
sources $s$ and currents $\vect{c}$:
\begin{gather*}
  \vect{\nabla}\cdot\vect{P} = s = -\nabla^2 \phi, \qquad
  \vect{\nabla}\times\vect{P} = \vect{c} = \vect{\nabla}\times(\vect{\nabla}\times\vect{A}).
\end{gather*}
See [Helmholtz Decomposition][] for details.

(sec:SphericalCoordinates)=
## Spherical Coordinates

Our goal is to get quickly to the formula for things like the gradient and Laplacian in
spherical coordinates.
\begin{gather*}
  \vect{r} = \begin{pmatrix}
    x\\y\\z
  \end{pmatrix}
  =
  \begin{pmatrix}
    r\sin\theta\cos\phi\\
    r\sin\theta\sin\phi\\
    r\cos\theta
  \end{pmatrix},\qquad
  r\in[0, \infty), \qquad
  \theta \in [0, \pi], \qquad
  \phi \in [0, 2\pi).
\end{gather*}
One easy approach is to compute the Jacobian matrix for the coordinate transformation
\begin{gather*}
  \d{\vect{r}} = \mat{J}\cdot
  \begin{pmatrix}
    \d{r}\\
    \d{\theta}\\
    \d{\phi}
  \end{pmatrix},\qquad
  \mat{J} = \begin{pmatrix}
     \sin\theta\cos\phi & r\cos\theta\cos\phi & -r\sin\theta\sin\phi\\
     \sin\theta\sin\phi & r\cos\theta\sin\phi & r\sin\theta\cos\phi\\
     \cos\theta & -r\sin\theta & 0
  \end{pmatrix}.
\end{gather*}
:::{margin}
*Hint: expand the determinant using the bottom row.*
:::
The volume element follows from the determinant of this matrix
\begin{gather*}
  \d^3\vect{r} = \d{x}\d{y}\d{z} = \det(\mat{J})\;\d{r}\d{\theta}\d{\phi}
  = r^2\sin\theta \; \d{r}\d{\theta}\d{\phi}
  = -r^2\d{r}\;\d{(\cos\theta)}\;\d{\phi}.
\end{gather*}
:::{margin}
*Hint: Remember that it is diagonal.  This means the columns of $\mat{J}$ are
orthogonal, which is easy to check.*
:::
Similarly, the metric tensor is
\begin{gather*}
  \mat{g} = \mat{J}^T\mat{J} = \begin{pmatrix}
    1 & 0 & 0\\
    0 & r^2 & 0\\
    0 & 0 & r^2\sin^2\theta
  \end{pmatrix},
\end{gather*}
so that
\begin{gather*}
    \d{\vect{r}}^T\d{\vect{r}} = 
      \begin{pmatrix}
    \d{r} &
    \d{\theta} &
    \d{\phi}
  \end{pmatrix}
  \mat{g}
  \begin{pmatrix}
    \d{r}\\
    \d{\theta}\\
    \d{\phi}
  \end{pmatrix}.
\end{gather*}




















[Helmholtz decomposition]: <https://en.wikipedia.org/wiki/Helmholtz_decomposition>
