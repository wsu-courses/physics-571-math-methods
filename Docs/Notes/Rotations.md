---
jupytext:
  formats: ipynb,md:myst,py:light
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup; mmf_setup.nbinit()
import os
from pathlib import Path
FIG_DIR = Path(mmf_setup.ROOT) / '../Docs/_build/figures/'
os.makedirs(FIG_DIR, exist_ok=True)
import logging; logging.getLogger("matplotlib").setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
try: from myst_nb import glue
except: glue = None
```

(sec:Rotations)=
Rotations
=========

:::{margin}
The notation $\mat{\vect{\theta}\times}$ might be a bit unusual, but it emphasizes that
this is a matrix -- a linear operator which takes vectors into vectors:
\begin{multline*}
  \vect{\theta}\times(\alpha \vect{A} + \beta \vect{B})
  = \\
  \alpha \vect{\theta}\times\vect{A} + \beta \vect{\theta}\times\vect{B}.
\end{multline*}
Note that
\begin{gather*}
  [\vect{A}\times\vect{B}]_c = \varepsilon_{abc}A_aB_b = [\vect{A}\times]_{cb}B_{b},\\
  \varepsilon_{abc}A_a = [\vect{A}\times]_{cb}.
\end{gather*}
:::
Here we discuss the structure of rotations in 2D and 3D.  In 3D, we shall represent
these matrices as
\begin{gather*}
  \mat{R}_{\vect{\theta}} = e^{\mat{\vect{\theta}\times}}, \qquad
  [\mat{\theta}\times]_{ab} = \theta_k\varepsilon_{kba},
\end{gather*}
corresponding to the 3D matrix that actively rotates vectors about the axis defined by
$\vect{\theta}$ in a right-handed sense, rotating through a total angle of $\theta =
\norm{\vect{\theta}}$.  The set of all rotation matrices
$\{\mat{R}_{\vect{\theta}}\mid \vect{\theta}\in \mathbb{R}^3\}$
with matrix multiplication as the binary operation form a **Lie group**, and the set of
all exponents $\{\mat{\vect{\theta}\times}\mid \vect{\theta}\in \mathbb{R}^3\}$ forms
the corresponding **Lie algebra**.

## Warmup: Rotations in 2D
To better understand rotations, we start from 2D.  Let $\mat{R}_{\theta}$ be the matrix
that effects an active counter-clockwise rotation through angle $\theta$.  This must
take
\begin{gather*}
  \mat{R}_{\theta}
  \begin{pmatrix}
    1\\
    0
  \end{pmatrix}
  =
  \begin{pmatrix}
    \cos \theta\\
    \sin \theta
  \end{pmatrix}, \qquad
  \mat{R}_{\theta}
  \begin{pmatrix}
    0\\
    1
  \end{pmatrix}
  =
  \begin{pmatrix}
    -\sin \theta\\
    \cos \theta
  \end{pmatrix}.
\end{gather*}
Hence, we must have
\begin{gather*}
  \mat{R}_{\theta} = \begin{pmatrix}
    \cos \theta & -\sin \theta\\
    \sin\theta & \cos \theta
  \end{pmatrix}.
\end{gather*}
The set of all such matrices corresponds to the group $SO(2)$ of special orthogonal
matrices in 2D.

From the {ref}`sec:Assignment1` we saw that this can be obtained by exponentiating
another matrix
\begin{gather*}
  \mat{R}_{\theta}
  = \exp\left\{\theta\begin{pmatrix}
    0 & 1\\
    - 1 & 0 
  \end{pmatrix}\right\}.
\end{gather*}
Note that each matrix $\mat{R}_{\theta}$ can be fully described by the angle $\theta \in
[0, 2\pi)$.  Thus, the structure of the **group** of rotations is isomorphic to a circle
which we can describe as the set $\{e^{\I\theta} \mid \theta \in [0, 2\pi)\}$.  This
corresponds to the group $U(1)$ of unitary matrices in 1D.  Thus $SO(2) \cong U(1)$: the
to groups are **isomorphic**, meaning there is a one-to-one relationship between the two
that preserves the group structure.

The corresponding **Lie algebra** consists of the matrices $\{\theta \mat{T} \mid \theta
\in \mathbb{R}\}$ where
\begin{gather*}
  \mat{T} = \begin{pmatrix}
    0 & 1\\
    -1 & 0
  \end{pmatrix}, \qquad
  \mat{T}^{2} = - \mat{1}.
\end{gather*}
The structure of this algebra $\mathfrak{so}(2)$ is that of the real line
$\mathcal{R}$.  Note that it is isomorphic to the algebra $\mathfrak{u}(1)$
corresponding to $U(1)$ with $\mat{T} \equiv \I$ playing the role of the imaginary
number.

```{code-cell}
:tags: [margin, hide-input]
fig, ax = plt.subplots(figsize=(4, 2))
th = np.linspace(0, 2*np.pi, 200)
ax.plot(np.cos(th), 1+np.sin(th), '-C0', lw=2, label="Lie group")
th = np.linspace(-2, 4)
ax.plot(th, 0*th, '-C1', lw=2, label="Lie algebra")
ax.legend()
ax.set(aspect=1, xlim=[-2, 4], xticks=[-2, -1, 0, 1, 2, 3, 4], 
       xticklabels=[], yticks=[0, 1, 2], yticklabels=[]);
```
The geometry of this is shown in the figure to the right: the group is a circle, but
locally at the identity, it looks like a straight line.  More generally, note that
locally at *every point* it looks like a straight line.  The structure of the algebra
describes the structure of the tangent space at all points of the group.  Note that
*exponentiating* points in the algebra give us the group, which, in this case, is **compact**. 

## 3D Rotations
Generalizing to 3D we have, for rotations in the $x$-$y$ plane:
\begin{gather*}
  \mat{R}_{\theta\uvect{z}} = \begin{pmatrix}
    \cos \theta & -\sin \theta & 0\\
    \sin\theta & \cos \theta & 0 \\
    0 & 0 & 1
  \end{pmatrix}
  = \exp\Biggl\{\theta
  \underbrace{
    \begin{pmatrix}
      0 & 1 & 0\\
      - 1 & 0 & 0\\
      0 & 0 & 0
    \end{pmatrix}}_{\mat{T}_z}
  \Biggr\}
  = e^{\theta\mat{T}_z}
\end{gather*}
You should be able to convince yourself that rotations in the $y$-$z$ and $z$-$x$ planes
have the following form:
\begin{gather*}
  \mat{R}_{\theta\uvect{x}} = \begin{pmatrix}
    1 & 0 & 0\\
1    0 & \cos \theta & -\sin \theta\\
    0 & \sin\theta & \cos \theta
  \end{pmatrix}
  = \exp\left\{\theta\mat{T}_{x}\right\},\\
  \mat{R}_{\theta\uvect{y}} = \begin{pmatrix}
    \cos \theta & 0 & \sin \theta\\
    0 & 1 & 0\\
    -\sin\theta & 0 &\cos \theta
  \end{pmatrix}
  = \exp\left\{\theta\mat{T}_{y}\right\}.
\end{gather*}
More generally, we can rotate about any vector $\vect{\theta}$ by exponentiating
\begin{gather*}
  \mat{R}_{\vect{\theta}} = e^{\vect{\theta}\cdot\vect{\mat{T}}}, \qquad
  \vect{\theta}\cdot\vect{\mat{T}} = \theta_x\mat{T}_x + \theta_y\mat{T}_y + \theta_z\mat{T}_z
  = \mat{\vect{\theta} \times}.
\end{gather*}
If you compute the three matrices $\mat{T}_i$, you will find that they satisfy the
following commutation relations
\begin{gather*}
  [\mat{T}_x, \mat{T}_y] = -\mat{T}_z, \qquad
  [\mat{T}_y, \mat{T}_z] = -\mat{T}_x, \qquad
  [\mat{T}_z, \mat{T}_x] = -\mat{T}_y,
\end{gather*}
or more compactly, using the [Levi-Civita symbol][]
\begin{gather*}
  [\mat{T}_i, \mat{T}_j] = -\varepsilon_{ijk}\mat{T}_k.
\end{gather*}
It turns out that, to rotate any physical object, one needs to find appropriate matrices
that satisfy this relationship.  Once you find these, you can determine how the object
rotates.
      
In {ref}`sec:Assignment2`, you are asked to find a set of 2D and 3D matrices that
satisfy this relationship.  Exponentiating the 2D matrices will tell you how 2-component
objects called **spinors** (spin 1/2) or **qubits** transform under rotations.  The 3D
matrices correspond to the rotations shown here.

Note the structure here: each element $\mat{R}_{\vect{\theta}}$ of the group $SO(3)$ can
be described uniquely by the vector $\vect{\theta} \in \mathbb{R}^3$ with lengths
$\theta \in [0, 2\pi)$.  This is a solid sphere or radius $2\pi$ with the outer shell
identified with the point at the origin $\vect{\theta} = \vect{0}$.  While this
embedding in $\mathbb{R}^3$ gives the appropriate **topology** of $SO(3)$, it does not
give an accurate representation of the structure.  Locally, every point in this group
looks like $\mathbb{R}^3$ -- a real three-dimensional vector space spanned by the three
basis "vectors" -- the matrices $\mat{T}_{x,y,z}$, which are called the **generators**.

To see the relevance of the commutation relations, let's look at rotations through small
angles:
\begin{gather*}
  \mat{R}_{\vect{\theta}} = \mat{1} + \vect{\theta}\cdot\vect{\mat{T}} + O(\theta^2).
\end{gather*}
Consider what happens when we combine two rotations:
\begin{gather*}
  \mat{R}_{\vect{\phi}}\mat{R}_{\vect{\varphi}}=\mat{R}_{\vect{\theta}},\qquad
  e^{\vect{\phi}\cdot\vect{\mat{T}}}
  e^{\vect{\varphi}\cdot\vect{\mat{T}}}
  =
  e^{\vect{\theta}\cdot\vect{\mat{T}}}.
\end{gather*}
In general, the relationship between $\vect{\theta}$ and the original rotations
$\vect{\phi}$ and $\vect{\varphi}$ is quite complicated, but if the angles are
small, we can use the [Bake-Campbell-Hausdorff formula][BCH]:
\begin{gather*}
  \vect{\theta}\cdot\vect{\mat{T}} = 
  \vect{\phi}\cdot\vect{\mat{T}} 
  + \vect{\varphi}\cdot\vect{\mat{T}}
  + \frac{1}{2}[\vect{\phi}\cdot\vect{\mat{T}}, \vect{\varphi}\cdot\vect{\mat{T}}]
  + \cdots,
\end{gather*}
where all further terms contain nested commutators.  In order for the group to be
closes, we ensure that all terms on the right lie in the algebra.  Thus, the generators
must be closed under commutation:
\begin{gather*}
  [\mat{T}_{i}, \mat{T}_{j}] = \sum_{k}f_{ijk}\mat{T}_{k},
\end{gather*}
so that
\begin{gather*}
  \theta_k = \phi_k + \varphi_k + \sum_{ij}\frac{f_{ijk}\phi_i\varphi_j}{2} + \dots.
\end{gather*}
Note that the product of the matrices need not lie in the algebra: it only needs to be
closed under commutation.  For the rotation group $SO(3)$, we have $f_{ijk} =
-\varepsilon_{ijk}$, and so
\begin{gather*}
  \vect{\theta} = \vect{\phi} + \vect{\varphi} -  \frac{\vect{\phi}\times\vect{\varphi}}{2} + \dots.
\end{gather*}


```{code-cell}
:tags: [hide-input]
from scipy.linalg import expm
rng = np.random.default_rng(seed=2)
h = 0.01
phi1, phi2 = h * rng.random(size=(2, 3))
theta = phi1 + phi2 - np.linalg.cross(phi1, phi2)/2
eps = np.zeros((3, 3, 3))
eps[0,1,2] = eps[1, 2, 0] = eps[2, 0, 1] = 1
eps[2,1,0] = eps[1, 0, 2] = eps[0, 2, 1] = -1
R1 = expm(-np.einsum('k,kba->ab', phi1, eps))
R2 = expm(-np.einsum('k,kba->ab', phi2, eps))
R3 = expm(-np.einsum('k,kba->ab', theta, eps))
assert np.allclose(R1@R2, R3)
```

## Quantum Mechanics

You will learn in classical mechanics that there is a deep connection through [Noether's
theorem][] between continuous
symmetries -- like time-translation invariance, translation invariance, and rotational
invariance -- and conservation laws:
* **Time translation** invariance implies conservation of **energy**. *(Actually
  conserved hamiltonian, but this is usually the energy.)*
* **Space translation** invariance implies conservation of **momentum**.
* **Rotational** invariance implies conservation of **angular momentum**.

These are codified in quantum mechanics as follows:
> The operators corresponding to the conserved quantities form a basis for the **Lie
> algebra**, and exponential to effect the transformations of the **Lie group** on the
> wavefunction.
>
> \begin{align*}
    \renewcommand{\op}[1]{\hat{\mathbf{#1}}}
    \braket{\vect{x}|e^{\vect{\lambda}\vect{\op{p}}/\I\hbar}|\psi} 
       &= \psi(\vect{x}-\vect{\lambda}),\\
    \braket{\vect{x}|e^{\vect{\theta}\cdot\vect{\op{L}}/\I\hbar}|\psi}
       &= \psi\bigl(\mat{R}^{-1}_{\vect{\theta}}\vect{x}\bigr),\\
     e^{t\op{H}/\I\hbar}\ket{\psi(t_0)}
       &= \ket{\psi(t_0 + t)}.
  \end{align*}

### Space Translations

We start with translation and conservation of momentum in one dimension.  Elements of
the algebra have the form $\lambda \op{p}$, which effect translations as
\begin{gather*}
  \braket{x|e^{\lambda \op{p}/\I\hbar}|\psi} = \psi(x-\lambda).
\end{gather*}
In other words, we say that $\op{p}/\I\hbar$ **generates** translations of the wavefunction to the
right.

To see this, note that the momentum operator behaves like
\begin{gather*}
  \braket{x|\op{p}|\psi} = -\I\hbar \pdiff{\psi(x)}{x},
\end{gather*}
thus, expanding the exponential, we recognize the Maclaurin series expansion of
$\psi(x-\lambda)$ about $x$:
\begin{align*}
  \braket{x|e^{\lambda \op{p}/\I\hbar}|\psi} &=  
  e^{-\lambda\partial/\partial x}\psi(x)\\
  &= \left(
    1 - \lambda \pdiff{}{x} + \frac{\lambda^2}{2!}\pdiff[2]{}{x} - \cdots
  \right)\psi(x)\\
  &= \psi(x) - \lambda \psi'(x) + \frac{\lambda^2}{2!}\psi''(x) - \cdots
  = \psi(x-\lambda).
\end{align*}
Everything works in higher dimensions
\begin{gather*}
  \braket{\vect{x}|e^{\vect{\lambda}\cdot\vect{\op{p}}/\I\hbar}|\psi} 
  = \psi(\vect{x}-\vect{\lambda}).
\end{gather*}

### Rotations

The angular momentum operator is
\begin{gather*}
  \vect{\op{L}} = \vect{\op{r}}\times\vect{\op{p}}, \qquad
  \vect{\theta}\cdot\vect{\op{L}} = \varepsilon_{abc}\theta_a \op{x}_b\op{p}_c
  = (\vect{\theta}\times\vect{\op{x}})\cdot\vect{\op{p}}.
\end{gather*}
Expanding as before, we have
\begin{align*}
  \braket{\vect{x}|e^{\vect{\theta}\cdot\vect{\op{L}}/\I\hbar}|\psi}
  &= \psi(\vect{x}) - (\vect{\theta}\times\vect{x})\cdot\vect{\nabla}\psi(\vect{x})
  + \frac{(\vect{\theta}\times\vect{x})\cdot\vect{\nabla}\Bigl((\vect{\theta}\times\vect{x})\cdot\vect{\nabla}\psi(\vect{x})\Bigr)}{2!} - \cdots
  = \psi(x-\lambda).
\end{align*}











[BCH]: <https://en.wikipedia.org/wiki/Baker%E2%80%93Campbell%E2%80%93Hausdorff_formula>
[Levi-Civita symbol]: <https://en.wikipedia.org/wiki/Levi-Civita_symbol>
[Noether's theorem]: <https://en.wikipedia.org/wiki/Noether%27s_theorem>
