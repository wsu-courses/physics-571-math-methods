---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (phys-571)
  language: python
  name: phys-571
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment7)=
# Assignment 7: Complex Analysis

**Due Fri 25 Oct 2024 at the start of class**

## 1. Complex Numbers
:::{margin}
*For bonus, consider $e^{z} = (2.718\dots)^{z}$ as a multifunction, but only for part 3.
Don't do this for 4.*
:::
Find the Cartesian form for **all values** of
1. $(-8)^{1/3}$.
2. $\I^{1/4}$.
3. $e^{\I \pi/4}$.
4. $\cos(1+\pi \I)$.

Sketch their locations in the complex plane and circle (or put a box around) the
principal value returned by the corresponding function in Python.

## 2. Analytic Functions

Find the analytic function $f(z) = u(x,y) + \I v(x,y)$
1. if $u(x, y) = x^3 - 3xy^2$,
2. if $v(x, y) = e^{-y}\sin x$.

Express your answers in terms of $z$. *(I.e. show that $f(z)$ has no dependence on $z^*$.)*

Based on your work, do you think we can choose any $u(x, y)$ and expect to be able to find a $v(x,
y)$ that makes $f(z)$ analytic?   If not, give an example of a restrictive condition
$u(x, y)$ must satisfy.

## 3. A Simple Contour Integral

Evaluate the following contour integral
\begin{gather*}
  \oint_{C} \frac{\d{z}}{z^2-1}  
\end{gather*}
counter-clockwise over the contour $C$ where $C$ is the circle $\abs{z-1} = 1$.

## 4. A Box Contour

Evaluate the following contour integral
\begin{gather*}
\oint_{C} \frac{e^{\I z}}{z^3}\d{z}
\end{gather*}
counter-clockwise over a square with sides of length $a > 1$ centered at $z=0$.

## 5. Laurent Series

Obtain the Laurent expansion of the following about $z=1$:
\begin{gather*}
  \frac{ze^{z}}{z-1}.
\end{gather*}

## 6. Check Your Results

Check your results numerically.  For example:
* 1. Evaluate the various quantities with python.
* 2. Numerically check your expressions to check your algebra.
* 3. and 4. Explicitly formulate the contour integrals, and then integrate with `quad`.
* 5. Numerically sum the series close to $z=1$ to make sure you get the correct answer.
