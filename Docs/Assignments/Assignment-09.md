---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (phys-571)
  language: python
  name: phys-571
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment9)=
# Assignment 9: Fourier Transforms

**Due Fri 8 Nov 2024 at the start of class**

## 1. Fourier Series

Find the Fourier series representation of the periodic function $f(x+2\pi) = f(x)$ where
\begin{gather*}
  f(x) = \begin{cases}
    0 & -\pi < x \leq 0\\
    x & 0 \leq x < \pi.
  \end{cases}
\end{gather*}
Sketch this function bye hand and check your result numerically.

From this Fourier expansion, show that
\begin{gather*}
  \frac{\pi^2}{8} = 1 + \frac{1}{3^2} + \frac{1}{5^2} + \cdots.
\end{gather*}

## 2. Fourier Transform

Find the Fourier transform of the following triangular pulse:
\begin{gather*}
  f(x) = \begin{cases}
    h(1-a\abs{x}) & \abs{x} < 1/a,\\
    0  & \text{otherwise}.
  \end{cases}
\end{gather*}
Sketch this function by hand.

Show that, setting $h=a$ and taking the limit
$a\rightarrow \infty$ gives the Fourier transform of the Dirac delta function, hence this
provides another sequence that approaches $f(x) \rightarrow \delta(x)$.

## 3. Neutron Diffusion

The 1D neutron diffusion equation with a (plane) source is
\begin{gather*}
  - D\diff[2]{\phi(x)}{x} + K^2 D \phi(x) = Q \delta(x)
\end{gather*}
where $\phi(x)$ is the neutron flux, $Q\delta(x)$ is the (plane) source at $x=0$, and
$D$ and $K^2$ are constants.  Apply a Fourier transform.  Solve the equation in Fourier
space, then transform your solution back to position space.

## 4. Poisson's Equation

Solve Poisson's equation
\begin{gather*}
  \nabla^2 \psi(\vect{r}) = - \frac{\rho(\vect{r})}{\epsilon_0}
\end{gather*}
by taking the Fourier transform of both sides of the equation, solving for the Fourier
transform of $\psi(\vect{r})$ and then applying the inverse Fourier transform to find
the solution in position space.

## 5. Laplace Transform

Find the Laplace transform of the square wave (with period $a$) defined by
\begin{gather*}
  F(t) = \begin{cases}
    1 & 0 < t < \frac{a}{2}\\
    0 & \frac{a}{2} < t < a
  \end{cases}, \qquad
  F(t + a) = F(t)
\end{gather*}
