---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (phys-571)
  language: python
  name: phys-571
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment4)=
# Assignment 4: Tensors

**Due Fri 27 Sep 2024 at the start of class**

## 1. Antisymmetric Tensors

:::{margin}
Recall that
\begin{gather*}
  \varepsilon_{abk}\varepsilon^{cdk} = \delta^{c}_{a}\delta^{d}_{b} - \delta^{c}_{b}\delta^{d}_{a}.
\end{gather*}
Use this to derive an expression for $\varepsilon_{aij}\epsilon^{bij}$ to help you.
:::
Consider a real antisymmetric tensor $B^{ij} = -B^{ji}$ in 3D.  Count the degrees of freedom, and
show that any such tensor can be written 
\begin{gather*}
  B^{mn} = \varepsilon^{mnk}A_k.
\end{gather*}
Write out $B^{mn}$ explicitly as a matrix showing where the three components $A_{1}$,
$A_{2}$, and $A_{3}$ go, then use the properties of the Levi-Civita symbol to show that
\begin{gather*}
  A_k = \tfrac{1}{2}\varepsilon_{ijk}B^{ij}.
\end{gather*}

This is important for embedding the electromagnetic field $\vect{B}$ into the
[electromagnetic field tensor][] $F^{\mu\nu}$.

## 2. Diagonal Metric

If the covariant basis vectors $\boldsymbol{\varepsilon_{i}}$ are orthogonal, show that:
* $g_{ij}$ is diagonal.
* $g^{ii} = 1/g_{ii}$ (no summation).
* $\norm{\boldsymbol{\varepsilon^{i}}} = 1/\norm{\boldsymbol{\varepsilon_{i}}}$.

:::{note}
The notation here is from Arfken, but differs slightly from what I have been using in
class and in these notes in {ref}`sec:Tensors`.  In these notes, I have used the
notation $\ket{X_{\alpha}} \equiv \boldsymbol{\varepsilon_{i}}$ for the basis vectors and
$\ket{X^{\alpha}}\equiv \boldsymbol{\varepsilon^{i}}$ for the contravariant basis
vectors, though, as I argue there, the latter are quite **unnatural** and should only be
used when absolutely needed.
:::

## 3. Covariant Derivative

Verify that $V_{i;j} = g_{ik}V^{k}_{;j}$ by showing that
\begin{gather*}
  \pdiff{V_{i}}{q^j} - V_k\Gamma^{k}_{ij} = g_{ik}\left[
    \pdiff{V^{k}}{q^j} + V^{m}\Gamma^{k}_{mj}
  \right].
\end{gather*}
I.e., show that the correction piece in the covariant derivative changes sign when
differentiating vectors (positive sign) vs covectors (negative sign).

## 4. Christoffel Symbols

Consider a two-dimensional space defined by the surface of a sphere of radius $r$.  The
square of the line element is given by
\begin{gather*}
  \d{s}^2 = r^2\;\d\theta^2 + r^2\sin^2\theta\;\d\phi^2.
\end{gather*}

1. Determine the coefficients of the metric tensor in both covariant form, $g_{ij}$
   and contravariant form $g^{ij}$.
2. Evaluate all nonzero Christoffel symbols of the first and second kind.
3. Use these to show that

   \begin{gather*}
     L^2 = \vect{L}\cdot\vect{L} = -\frac{1}{\sin\theta}\pdiff{}{\theta}\left(\sin\theta
     \pdiff{}{\theta}\right) - \frac{1}{\sin^2\theta}\pdiff[2]{}{\phi}
   \end{gather*}
   
   from last week by directly acting $\vect{L}$ on a test function twice:
   \begin{gather*}
     \vect{L} = \I\left(
       \ket{\hat{e}_{\theta}}\frac{1}{\sin\theta}\pdiff{}{\phi} - 
       \ket{\hat{e}_{\phi}}\pdiff{}{\theta}
     \right).
   \end{gather*}

## 5. Jacobians

For the transformation (with $x\geq 0$ and $y \geq 0$)
\begin{gather*}
  u = x + y, \qquad
  v = x/y,
\end{gather*}
compute the following Jacobian in two ways:
\begin{gather*}
  \mat{J} = \pdiff{(x, y)}{(u, v)}
\end{gather*}
1. By direct computation.
2. By first computing $\mat{J}^{-1}$.
3. Compute the induced metric $g_{ij}$ in the coordinates $(u, v)$.

```{code-cell}
:tags: [margin, hide-input]
L = 5
dx = 0.2
Nx = 100
N = (L//dx)*Nx
x = np.arange(N+1) * dx / Nx + 0.1
x, y = np.meshgrid(x, x, indexing='ij', sparse=True)
z = x+1j*y
w = x + y + 1j*x/y

plt.clf();plt.close('all')
fig, ax = plt.subplots()

def plot(z, ax, fmt='-k', **kw):
    ax.plot(z[::Nx,:].T.real, z[::Nx,:].T.imag, fmt, **kw)
    ax.plot(z[:, ::Nx].real, z[:, ::Nx].imag, fmt, **kw)

plot(z, ax, '-k', lw=1)
plot(w, ax, '-C0', lw=1)
ax.set(aspect=1, xlim=(0,5), ylim=(0,5));
```

[electromagnetic field tensor]: <https://en.wikipedia.org/wiki/Electromagnetic_tensor>


