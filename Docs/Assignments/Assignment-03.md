---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (phys-571)
  language: python
  name: phys-571
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment3)=
# Assignment 3: Vector Calculus

**Due Wed 18 Sep 2024 at the start of class**

:::{margin}
Hint: Use the Levi-Civita symbol and the fact that
\begin{gather*}
  [\vect{A}\times\vect{B}]_c = \varepsilon_{abc}A_aB_b
\end{gather*}
(with summation implied over indices $a$ and $b$), and the property that cyclic
permutations are even, so that
\begin{gather*}
  \varepsilon_{abc} = \varepsilon_{bca} = \varepsilon_{cab} =\\
  = - \varepsilon_{acb} = -\varepsilon_{bac} = -\varepsilon_{cba}.
\end{gather*}
:::
## 1. Vector Manipulations

Express $\vect{\nabla}\cdot(\vect{a}\times\vect{b})$ in terms of the curls
$\vect{\nabla}\times\vect{a}$ and $\vect{\nabla}\times\vect{b}$. *(Check your result
explicitly with some simple vector fields $\vect{a}$ and $\vect{b}$.)*

:::{margin}
*Based on Arfken, 3.7.2.*
:::
## 2. Non-conservative Work

Find the work $\int \vect{F}\cdot\d{\vect{r}}$ done by moving on a unit circle in the
$x$-$y$ plane, doing work **against** a force field given by
\begin{gather*}
  \vect{F} = \frac{-y\uvect{i}}{x^2+y^2} + \frac{x\uvect{j}}{x^2+y^2}
\end{gather*}
1. The work $W_1$ done moving counterclockwise from $0$ to $\pi$.
2. The work $W_2$ done moving clockwise from $0$ to $-\pi$.
3. Explain why the work depends on the path by breaking $\vect{F} = -\vect{\nabla} \phi +
   \vect{\nabla}\times \vect{A}$ using the Helmholtz decomposition.  (Use an educated
   guess, don't actually try to evaluate the integrals, but find an explicit example of
   potentials $\phi$ and $\vect{A}$ that give $\vect{F}$.

## 3. Maxwell's Equations

Derive the integral form of Maxwell's equations from the following differential form by
applying the appropriate integral theorem and explain physically the meaning of each:
\begin{align*}
  \vect{\nabla}\cdot\vect{B} &= 0,\\
  \vect{\nabla}\cdot\vect{E} &= \frac{\rho}{\epsilon_0},\\
  \vect{\nabla}\times\vect{B} &= \epsilon_0\mu_0 \pdiff{\vect{E}}{t} + \mu_0 \vect{J},\\
  \vect{\nabla}\times\vect{E} &= -\pdiff{\vect{B}}{t}.
\end{align*}

Show that Maxwell's equations have solutions that propagate at the speed of light $c =
1/\sqrt{\epsilon_0\mu_0}$.

## 4. Net Current
:::{margin}
See Arfkin 3.8.5. *Hint: consider each component of $\vect{J}$ separately and use
Maxwell's equations to show that $J_i = \vect{\nabla}\cdot(x_i\vect{J})$.  Then apply
Gauss's theorem.*
:::
A particular steady-state electric current distribution is localized in space.  Choosing
a bounding surface far enough out so that the current density $\vect{J} = \vect{0}$
everywhere on the surface, show that the volume integral of the current is zero:
\begin{gather*}
  \int \vect{J}\d{\tau} = \vect{0}.
\end{gather*}

(sec:A3-5)=
## 5. Angular Momentum
:::{margin}
See Arfkin 3.10.32.  *Please work through as many of the problems 3.10.28 through 3.10.34 as
you need to be comfortable with the angular momentum operator: you will be expected to
know this for quantum mechanics.*
:::
1. Show that (setting $\hbar=1$)
   \begin{gather*}
     \vect{L} = -\I (\vect{r}\times\vect{\nabla}) = \I\left(
     \uvect{e}_\theta \frac{1}{\sin\theta}\pdiff{}{\phi} -
     \uvect{e}_{\phi}\pdiff{}{\theta}
   \right)
   \end{gather*}
   *Note that $\vect{L}$ only looks at angular variations.*
2. From $L^2 = \norm{\vect{L}}_2^2 = L_x^2 + L_y^2 + L_z^2$ show that
   \begin{align*}
     L^2 &= -\frac{1}{\sin\theta}\pdiff{}{\theta}\left(
     \sin\theta \pdiff{}{\theta}\right) - \frac{1}{\sin^2\theta}\pdiff[2]{}{\phi}\\
     &=-r^2\nabla^2 + \pdiff{}{r}\left(r^2\pdiff{}{r}\right).
   \end{align*}
   *Note: Although it is straightforward to do this using Cartesian coordinates, it is
   somewhat messy. Feel free to explore other techniques if you like.*
:::{note}
This last relation is critical for solving problems like the hydrogen atom in quantum
mechanics.  It allows us to replace the Laplacian in the Schrödinger equation with
\begin{gather*}
  -\nabla^2 = \frac{L^2}{r^2} - \frac{1}{r^2}\pdiff{}{r}\left(r^2 \pdiff{}{r}\right),
\end{gather*}
allowing us to write solutions in terms of spherical harmonics, which are eigenfunctions
of $L^2$:
\begin{gather*}
  \psi(\vect{r}) = f(r)Y^{m}_{\ell}(\theta,\phi), \qquad
  L^2Y^{m}_{\ell}(\theta,\phi) = \ell(\ell+1)Y^{m}_{\ell}(\theta,\phi),\\
  -\nabla^2\psi(\vect{r}) = Y^{m}_{\ell}(\theta,\phi)\left(
    \frac{\ell(\ell+1)}{r^2} - \frac{1}{r^2}\pdiff{}{r}\left(r^2 \pdiff{}{r}\right)\right)f(r).
\end{gather*}
We can thus focus on the radial form of the solution, which picks up the additional
"centrifugal" potential $\hbar^2 \ell(\ell+1)/r^2$.
:::


[dilogarithm]: <https://en.wikipedia.org/wiki/Dilogarithm>
[polylogarithm]: <https://en.wikipedia.org/wiki/Polylogarithm>
[proof of the Euler product formula]: <https://en.wikipedia.org/wiki/Proof_of_the_Euler_product_formula_for_the_Riemann_zeta_function>
[PDF]: <https://en.wikipedia.org/wiki/Probability_density_function>
