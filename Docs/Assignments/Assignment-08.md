---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (phys-571)
  language: python
  name: phys-571
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment8)=
# Assignment 8: Contour Integration

**Due Fri 1 Nov 2024 at the start of class**

:::{margin}
Recall that the residue is
\begin{gather*}
  \mathrm{Res}(f, z_0) = \oint_{C} f(z) \frac{\d{z}}{2\pi \I}
\end{gather*}
where $C$ is a positively oriented contour around $z_0$ not enclosing any other
singularities.
:::
## 1. Residues
Determine the types of singularities in each of the following and compute the residue.
(Assume $a>0$ is real and positive.)
\begin{align*}
  A(z) &= \frac{1}{z^2+a^2} &
  B(z) &= \frac{1}{(z^2+a^2)^2}\\
  C(z) &= \frac{z^2}{z^2+a^2} &
  D(z) &= \frac{\sin(1/z)}{z^2+a^2}\\
  E(z) &= \frac{ze^{+\I z}}{z^2+a^2} &
  F(z) &= \frac{ze^{+\I z}}{z^2-a^2}\\
  G(z) &= \frac{e^{+\I z}}{z^2-a^2} &
  H(z) &= \frac{z^{-k}}{z+1}, \quad 0 < k < 1
\end{align*}
  
## 2. Contour Integrals

Evaluate the following integrals using contours:
\begin{align*}
  I_1 &= \int_0^{2\pi}\frac{\cos 3\theta}{5-4\cos\theta}\d{\theta}, &
  I_2 &= \int_0^{\infty}\frac{x\sin x}{x^2+1}\d{x}, &
  I_3 &= \int_0^{\infty}\frac{x^p \ln x}{x^2 + 1}\d{x}, \quad 0 < p < 1.
\end{align*}

## 3. [Möbius Transformations][]
   
What part of the $z$-plane corresponds to the interior of the unit circle for each of
the following two conformal maps:
\begin{align*}
  w &= \frac{z-1}{z+1} &
  w &= \frac{z-\I}{z+\I}.
\end{align*}
Carefully sketch the behaviour of these maps in a manner similar to that shown in
{cite}`Needham:2023` or in my notes.  *(Choose lines or grids as needed to clearly
express the behaviour of the transformation, but be able to sketch these by hand as you
would need to in an exam setting.)*

[Möbius Transformations]: <https://en.wikipedia.org/wiki/M%C3%B6bius_transformation>
