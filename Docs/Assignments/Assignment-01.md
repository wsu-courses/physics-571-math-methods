---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.6
kernelspec:
  display_name: Python 3 (phys-571)
  language: python
  name: phys-571
---

```{code-cell}
:tags: [hide-cell]

import mmf_setup;mmf_setup.nbinit()
import logging;logging.getLogger('matplotlib').setLevel(logging.CRITICAL)
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt
```

(sec:Assignment1)=
# Assignment 1: Mathematical Preliminaries

**Due Fri 30 Aug 2024 at the start of class**

## 1. Series

Determine if the following series converge.  If they converge, find an expression for
the sum, or compute the sum to 12 digits of accuracy:
\begin{align*}
  \text{(a)} &\quad \sum_{n=2}^{\infty}\frac{1}{\ln n} &
  \text{(d)} &\quad \sum_{n=1}^{\infty}\frac{1}{\sqrt{n(n+1)}} \\
  \text{(b)} &\quad \sum_{n=1}^{\infty}\frac{n!}{10^n} &
  \text{(e)} &\quad \sum_{n=0}^{\infty}\frac{1}{2n^2+1},\\
  \text{(c)} &\quad \sum_{n=1}^{\infty}\frac{1}{n2^n} &
  \text{(f)} &\quad \sum_{n=1}^{\infty}\frac{(-1)^{n}}{n}.
\end{align*}

:::{margin}
Hint: One approach is to bound the tail of the series using integrals, then show that
the bounds converge.  Another hint is that $\gamma$ is the blue area in the following
figure:
![Euler's constant](https://upload.wikimedia.org/wikipedia/commons/0/04/Gamma-area.svg)
:::
## 2. Euler-Mascheroni Constant

Show that the following limit exists, and compute the **Euler-Mascheroni constant**
$\gamma$ to 6 digits (or 12 if you want more of a challenge):
\begin{gather*}
  \gamma = \lim_{N \rightarrow \infty}\left(
    \sum_{n=1}^{N}\frac{1}{n} - \ln N
  \right)
\end{gather*}


## 3. Sine and Cosine

By analogy with Euler's identity $e^{\I x} = \cos{x} + \I \sin{x}$, show using series that
\begin{gather*}
  \exp(\mat{I}x) = \mat{1}\cos{x} + \mat{I}\sin{x}, \qquad
  \mat{I}^2 = -\mat{1},
\end{gather*}
for any matrix that squares to the negative identity.  In particular
\begin{gather*}
  \exp\begin{pmatrix}
    0 & \theta\\
    -\theta & 0 
  \end{pmatrix}
  =
  \begin{pmatrix}
    \cos \theta & \sin\theta\\
    -\sin \theta & \cos \theta
  \end{pmatrix}.
\end{gather*}

```{code-cell}
# Check that I got the right signs etc.
import numpy as np
from scipy.linalg import expm
theta = 0.1
M = np.array([[0, theta], [-theta, 0]])
assert np.allclose(
    np.array([[np.cos(theta), np.sin(theta)],
              [-np.sin(theta), np.cos(theta)]]),
    expm(M))
```
## 4. Bose-Einstein Distribution (10 points)

Consider a thermal ensemble of 1D harmonic oscillator with quantized energies
\begin{gather*}
  E_n = \underbrace{\hbar \omega}_{\epsilon_0} (n + \tfrac{1}{2}).
\end{gather*}
At temperature $T$, each state is occupied with Maxwell-Boltzmann weight
\begin{gather*}
  f_T(E) \propto e^{-\beta E}, \qquad \beta = \frac{1}{k_B T}.
\end{gather*}
Thus, the average energy in the system is
\begin{gather*}
  \braket{E} = \frac{\sum_{n} E_nf_T(E_n)}{\sum_{n} f_T(E_n)}
\end{gather*}

Show that the zero-point energy $\hbar\omega/2$ drops out of the calculation, giving
the expression in Arfken's Exercise 1.3.14:
\begin{gather*}
  \braket{E} = \frac{\sum_{n=1}^{\infty} n \epsilon_0 e^{-\beta \epsilon_0 n}}
                    {\sum_{n=0}^{\infty} e^{-\beta \epsilon_0 n}}.
\end{gather*}
By identifying the numerator and denominator as binomal expansions, show that
\begin{gather*}
  \braket{E} = \frac{\epsilon_0}{e^{\beta \epsilon_0} - 1}.
\end{gather*}
Show that in the limit of high temperature $k_B T \gg \epsilon_0 = \hbar\omega$ we
obtain the classical result $\braket{E} \rightarrow k_B T = 1/\beta$. 

## 5. Vector Cross Products (5 points)

Prove that
\begin{gather*}
  (\vect{A}\times\vect{B})\cdot(\vect{A}\times\vect{B}) = (AB)^2 - (\vect{A}\cdot\vect{B})^2.
\end{gather*}

```{code-cell}
# Check that I got the right signs...
import numpy as np
rng = np.random.default_rng(seed=2)
A = rng.random(size=3)
B = rng.random(size=3)
assert np.allclose(
    np.dot(np.cross(A, B), np.cross(A, B)),
    (np.linalg.norm(A)*np.linalg.norm(B))**2 - np.dot(A, B)**2)
```
